<?php namespace App\Http\Controllers\v1;

use Laravel\Lumen\Routing\Controller as BaseController;
use Request;
use Config\Env;
use App\Models\Queue;
use App\Models\User\User;
use App\Models\User\Search;
use App\Models\Report;

class UserController extends BaseController
{
    
    public function getProfile(){
        $userIDs = Request::input('userIDs');
        if(is_string($userIDs)){
            $userIDs = explode(',', $userIDs);
        }
        $users = User::getUsers([], $userIDs);
        
        jsend(200, $users);
    }
    
    
    public function getMyProfile(){
        $userID = Request::input('userID');
        if( !($me = User::getByUserID($userID)) ){
            jsend(500, null, 'User not exists');
        }
        $me = User::format($me);
        jsend(200, $me);
    }
    
    public function album(){
        $targetUserID = Request::input('targetUserID') ?: Request::input('userID');
        $targetUid = User::getUid($targetUserID);
        if(!$targetUid || !$album = User::getAlbum($targetUid))
        {
            jsend(200, [], 'Album is empty');
        }
        else{
            jsend(200, $album, 'Success');
        }
    }
    
    /**
     * 虚拟货币或点数 的流向数值， 这里仅计算了正向给予的
     */
    public function circulationValue(){
        $fromUserID = Request::input('userID');
        $toUserID = Request::input('targetUserID');
        
        $fromUid = User::getUid($fromUserID);
        $toUid = User::getUid($toUserID);
        
        $value = User::getCirculationValue($fromUid, $toUid);
        
        jsend(200, ['value' => (int)$value], 'Success');
    }
    
    /**
     * 取得关注中的用户ID
     */
    public function followedUserIDs(){
        $userID = Request::input('userID');
        $IDs = User::getUserIDsFollowedTo($userID);
        
        jsend(200, $IDs, 'Success');
    }
    
    
    public function update(){
        if(!$userID = Request::input('userID')){
            jsend(500, null, 'Failture');
        }
        $values = [
            'nickname'          => ($v = Request::input('nickname')) === null ? null : htmlspecialchars($v, ENT_QUOTES),
            'pushNotification'  => ($v = Request::input('pushNotification')) === null ? null : (int)(bool)$v,
            'pushMessage'       => ($v = Request::input('pushMessage')) === null ? null : (int)(bool)$v,
            'pushInterest'      => ($v = Request::input('pushInterest')) === null ? null : (int)(bool)$v,
            'pushPostComment'   => ($v = Request::input('pushPostComment')) === null ? null : (int)(bool)$v,
            'constellation'     => User::fixConstellation(Request::input('constellation')),
            'birthday'  => ($v = trim(Request::input('birthday'))) && preg_match('/^\d{4}\-\d{2}-\d{2}$/i', $v) ? $v : date('Y').'-01-01',
            'gender'    => User::fixGender(Request::input('gender')),
            'height'    => (int)Request::input('height'),
            'weight'    => (int)Request::input('weight'),
            'avatar'    => htmlspecialchars(Request::input('picture'), ENT_QUOTES),
            'intro'     => htmlspecialchars(Request::input('intro'), ENT_QUOTES),
            'job'       => htmlspecialchars(Request::input('job'), ENT_QUOTES),
            'income'    => htmlspecialchars(Request::input('income'), ENT_QUOTES),
            'interest'  => htmlspecialchars(Request::input('interest'), ENT_QUOTES),
            'city'      => htmlspecialchars(Request::input('city'), ENT_QUOTES),
            //'loginTime' => Request::input('lastLogin'),
            'longitude'     => ($v = Request::input('longitude')) === null ? null : (float)$v,
            'latitude'      => ($v = Request::input('latitude')) === null ? null : (float)$v,
            'badge'         => ($v = Request::input('badge')) === null ? null : (int)$v,
            'badgeVisitor'  => ($v = Request::input('badgeVisitor')) === null ? null : (int)$v,
            'badgeFollow'   => ($v = Request::input('badgeFollow')) === null ? null : (int)$v,
            'badgeMutualFollow'   => ($v = Request::input('badgeMutualFollow')) === null ? null : (int)$v,
            'badgePost' => ($v = Request::input('badgePost')) === null ? null : (int)$v,
            'isHidden'  => (int)(bool)Request::input('isHidden'),
            'album'     => is_array($v = Request::input('album')) ? json_encode($v) : $v,
            'country'   => ($v = Request::input('country')) === null ? null : htmlspecialchars($v, ENT_QUOTES),
            'province'  => ($v = Request::input('province')) === null ? null : htmlspecialchars($v, ENT_QUOTES),
            'city'      => ($v = Request::input('city')) === null ? null : htmlspecialchars($v, ENT_QUOTES),
            'language'  => ($v = Request::input('language')) === null ? null : htmlspecialchars($v, ENT_QUOTES),
        ];
        $values = array_filter($values, function($v){ return $v !== null; });
        
        if(($v = Request::input('token')) !== null){
            $values['token'] = htmlspecialchars($v, ENT_QUOTES);
        }
                
        if($values && false !== User::update($userID, $values)){
            jsend(200, null, 'Success');
        }
        else{
            jsend(500, null, 'Failture');
        }
        
    }
    
    
    public function nearby(){
        $userID = Request::input('userID');
		$latitude = (float)Request::input('latitude');
		$longitude = (float)Request::input('longitude');
		$range = (float)Request::input('range', 5);
		$gender = User::fixGender(Request::input('gender')); // optional
		//$constellation = Request::input('constellation'); // optional, deprecated
		//$city = htmlspecialchars(Request::input('city'), ENT_QUOTES); // optional
		//$country = htmlspecialchars(Request::input('country'), ENT_QUOTES); // optional, v2, deprecated
		//$region = htmlspecialchars(Request::input('region'), ENT_QUOTES); // optional, v3
		$lowAge = max(0, (int)Request::input('lowAge', 12));
		$highAge = min(150, (int)Request::input('highAge', 60));
		$beforeTime = (int)Request::input('beforeTime', time()+86400);
		$newUserOnly = (int)Request::input('newUserOnly', 0); // optional, v2
		$count = min(200, (int)Request::input('count'));
        
        $users = Search::getNearby($count, $latitude, $longitude, $range, $gender, $lowAge, $highAge, $newUserOnly, $beforeTime);
        jsend(200, User::formatMultiple($users, [$userID]));
    }
    
    /**
     * 取得附近竞价排名的用户
     * 在限定的经纬度范围内， 且属于同一国家的用户将会被列出
     */
    public function nearbyBid(){
		$userID = Request::input('userID');
		$count  = (int)Request::input('count', 5);
		$country    = htmlspecialchars(Request::input('country'), ENT_QUOTES); 
		//$province    = htmlspecialchars(Request::input('province'), ENT_QUOTES);
		//$city    = htmlspecialchars(Request::input('city'), ENT_QUOTES);
		$latitude = (float)Request::input('latitude');
		$longitude = (float)Request::input('longitude');
        $range = (float)Request::input('range', 3);
        
        $users = Search::getNearbyBid($count, $country, $latitude, $longitude, $range, $userID);
        
        jsend(200, User::formatMultiple($users));
    }
    
    public function getByName(){
        $userID = Request::input('userID');
		$nickname = Request::input('nickname');
        
        $users = Search::searchName($nickname);
        
        jsend(200, User::formatMultiple($users));
        
    }
    
    private function _getPopular($scope = null){
        if(!in_array($scope, [Search::POPULAR_DAILY, Search::POPULAR_WEEKLY, Search::POPULAR_TOTAL])){
            return [];
        }
        
        $userID = Request::input('userID');
		$latitude   = is_numeric($latitude = Request::input('latitude')) ? $latitude : null;
		$longitude  = is_numeric($longitude = Request::input('latitude')) ? $longitude : null;
		$range = (float)Request::input('range', 10);
		$lowAge = (int)Request::input('lowAge', 12);
		$highAge = (int)Request::input('highAge', 50);
		$genders  = ($genders = Request::input('gender')) ? explode(',', $genders) : ''; // optional
		$constellation = Request::input('constellation'); // optional
		$city   = Request::input('city'); // optional
		$count = min(100, (int)Request::input('count', 10));
        
        $users = Search::getPopularUsers($scope, $count, $lowAge, $highAge, $latitude, $longitude, $range, $city, $genders, $constellation);
        
        jsend(200, User::formatMultiple($users));
    }
    
    
    public function dailyPopular(){
        $this->_getPopular(Search::POPULAR_DAILY);
    }
    
    public function weeklyPopular(){
        $this->_getPopular(Search::POPULAR_WEEKLY);
    }
    
    public function globalPopular(){
        $this->_getPopular(Search::POPULAR_TOTAL);
    }
    
    public function rank(){
		$mode = Request::input('mode');
		$country = htmlspecialchars(Request::input('country'), ENT_QUOTES);
		$province = htmlspecialchars(Request::input('province'), ENT_QUOTES);
		$city = htmlspecialchars(Request::input('city'), ENT_QUOTES);
		$count = min(100, abs(Request::input('count', 10)));
        
        $users = Search::rank($mode, $count, $country, $province, $city, 7*86400);
        jsend(200, User::formatMultiple($users));
    }
    
    
    public function getBlacklist(){
        $userID = Request::input('userID');
		$count = min(300, (int)Request::input('count', 10));
        
        $uid = User::getUid($userID);
        $users = User::getBlackList($uid, $count);
        jsend(200, User::formatMultiple($users));
    }
    
    public function addToBlacklist(){
        $userID = Request::input('userID');
        $banUserID = Request::input('blockedUserID');
        
        $uid = User::getUid($userID);
        $banUid = User::getUid($banUserID);
        // 不允许屏蔽官方账号
        if($banUserID != Env::OFFICIAL_USER_ID && User::addToBlacklist($uid, $banUid) ){
            jsend(200, null, 'Success');
        }
        else{
            jsend(500, null, 'Failture');
        }
    }
    
    public function deleteFromBlacklist(){
        $userID = Request::input('userID');
        $banUserID = Request::input('blockedUserID');
        
        $uid = User::getUid($userID);
        $banUid = User::getUid($banUserID);
        
        if( User::delFromBlacklist($uid, $banUid) ){
            jsend(200, null, 'Success');
        }
        else{
            jsend(500, null, 'Failture');
        }
    }
    
    public function userReportAction(){
        $syskey = Request::input('syskey');  //optional
        $informerUserID = Request::input('userID');
        $accusedUserID  = Request::input('reportedUserID');
        $reason         = htmlspecialchars(Request::input('reportedUserID', ''), ENT_QUOTES);  //optional
        $postId  = Request::input('postId');  //optional
        $commentId  = Request::input('commentId');  //optional
        $level = 1;
        
        if($syskey!==null && !Env::isValidAdminRequest($syskey)){
            jsend(403, null, 'Unauthorized Command!');
        }
        
        if($informerUserID == $accusedUserID){
            jsend(403, null, 'Wrong Action!');
        }
        
        $informerUid = User::getUid($informerUserID);
        $accusedUid = User::getUid($accusedUserID);
        if(!Report::add($informerUid, $accusedUid, $reason, $level, $postId, $commentId, ($syskey?1:0))){
            jsend(500, null, 'Failure!');
        }
        Report::checkAndBlockUser($accusedUid, 5, 10);
        jsend(200, null, 'Report is done!');
    }
    
    public function photoAcceptAction(){
        $userID = Request::input('userID');
        User::update($userID, ['isPhotoChecked' => 1]);
        jsend(200, null, 'Success');
        // TODO:
		//helper.sendPushNotification("", userID, PUSH_TYPE_PICTURE_ACCEPT);
    }
    
    public function photoRejectAction(){
        $userID = Request::input('userID');
        User::update($userID, ['isPhotoChecked' => 0]);
        // TODO:
		//helper.sendPushNotification("", userID, PUSH_TYPE_PICTURE_REJECT);
        
        \App\Libraries\Tools::POST('messageSendAction', [
			'adminKey'       => Env::SYS_REQUEST_KEY,
            'userID'         => Env::OFFICIAL_USER_ID,
            'receiverUserID' => $userID,
            'type'           => 'text',
            'content'        => trans('system.photo_reject'),
            'messageID'      => \App\Libraries\Generate::uuid()
        ]);
        
        jsend(200, null, 'Success');
        
    }
    
    public function isInTaiwan(){
        
    }
    
    /* == points && vips ========================================== */
    
    public function appDriverCallback(){
        $toUserID = Request::input('identifier'); // important
        $point = (int)Request::input('point', 0);// important
        
        if(User::givePoint($toUserID, $point)){
            jsend(200, null, 'Success');
        }
        else{
            jsend(500, null, 'Failure');
        }
    }
    
    public function tapjoyCallback(){
        $toUserID = Request::input('snuid'); // important
        $point = (int)Request::input('currency', 0);// important
        
        if(User::givePoint($toUserID, $point)){
            jsend(200, null, 'Success');
        }
        else{
            jsend(500, null, 'Failure');
        }
    }
    
    public function addPoint(){
        $toUserID = Request::input('userID'); // important
        $point = (int)Request::input('point', 0);
        if(User::givePoint($toUserID, $point)){
            jsend(200, null, 'Success');
        }
        else{
            jsend(500, null, 'Failure');
        }
    }
    
    public function addSvip(){
        $toUserID = Request::input('userID');
        $vipExtent = (int)Request::input('vipExtension', 0);
        
        if(User::giveVipAndPoint($toUserID, 0, 0, $vipExtent*86400)){
            jsend(200, null, 'Success');
        }
        else{
            jsend(500, null, 'Failure');
        }
    }
    
    /* ================================================= */
    
    public function getOfficialAccounts(){
        jsend(200, [ Env::OFFICIAL_USER_ID ], '');
    }
    
    public function getCountry(){
        $api = 'https://freegeoip.net/json/'.Request::getClientIp();
        $res = \App\Libraries\Tools::request($api, 'GET');
        if($res['code'] !== 200){
            jsend(500, null, 'failture');
        }
        /*
        $data = [
            'ip'           => '192.30.252.129',
            'country_code' => 'US',
            'country_name' => 'United States', 
            'region_code'  => 'CA', 
            'region_name'  => 'California', 
            'city'         => 'San Francisco', 
            'zip_code'     => '94107', 
            'time_zone'    => 'America/Los_Angeles', 
            'latitude'     => 37.77, 
            'longitude'    => -122.394, 
            'metro_code'   => 807
        ];
        */
        $data = json_decode($res['content'], true);
        jsend(200, ['country' => $data['country']]);
    }
    
    public function uploadMedia(){
        if(!($file = Request::input('myfile')) 
            || !isset($file['tmp_name']) 
            || !$file['tmp_name'])
        {
            jsend(500, null, 'Hey, gay! your mother let you go home for a meal!');
        }
        
        if($file['size'] > 104857600){ // 100M以上拒绝
            @unlink($file['tmp_name']);
            jsend(500, null, 'Denied because of size>100M!');
        }
        
        if(Env::IS_DEVELOPMENT === 1){
            $done = \App\Libraries\Tools::saveToLocal($file['tmp_name'], $file['name']);
        }
        else{
            $done = \App\Libraries\Tools::saveToS3($file['tmp_name'], $file['name']);
        }
        
        @unlink($file['tmp_name']);
        if($done){
            jsend(200, null, 'Success!');
        }
        else{
            jsend(500, null, 'Failure!');
        }
    }
    
}
