<?php namespace App\Http\Controllers\v1;

/**
 * 账号controller
 * @author d.x.c
 */

use Laravel\Lumen\Routing\Controller as BaseController;
use Request;
use Validator;
use App\Libraries\Generate;
use App\Libraries\Locale;
use App\Models\User\User;

use App\Models\Account\Account;
use App\Models\Device;

class AccountController extends BaseController
{
    
    public function register(){
        $validator = Validator::make(Request::input(), [
                'email' => 'Required|Max:50|Email',
                'password' => 'required|max:16|min:6|AlphaNum',
                'nickname' => 'required|max:12|min:1|',
                'gender' => 'required',
                'deviceID'  => 'required'
        ]);
        if($validator->fails()){
            $msgs = $validator->messages()->getMessages();
            $msgs = array_shift($msgs);
            return jsend(500, null, $msgs[0]);
        }
        
        $lang = Locale::fixLanguage(Request::input('language'));
        $deviceID = htmlspecialchars(Request::input('deviceID'), ENT_QUOTES);
        $userID = Generate::uuid();
        $accessToken = Generate::token();
        $reg = [
            'email'     => Request::input('email'),
            'password'  => md5(Request::input('password')),
            'nickname'  => strip_tags(Request::input('nickname')),
		    'userID'    => $userID,
        //    'accessToken'   => $accessToken,
		//    'deviceID'  => $deviceID,
		    'deviceType'=> htmlspecialchars(Request::input('deviceType'), ENT_QUOTES),
            'gender'    => ($v = Request::input('gender')) && in_array($v, ['male', 'female']) ? $v : '',
		    'birthday'  => ($v = trim(Request::input('birthday'))) && preg_match('/^\d{4}\-\d{2}-\d{2}$/i', $v) ? $v : date('Y').'-01-01' ,
		    'avatar'    => Request::input('avatar'),
		    'language'  => $lang,
		    'country'    => Locale::getCountryByLangCode($lang),
		    'point'     => Request::input('gender') == 'female' ? 10000 : 500,
            'isPhotoChecked'    => 1,   // 默认已验证照片
            'registerTime'  => time(),
        ];
        
        if(Account::exists($reg['email'])){
            return jsend(500, null, 'Email exists');
        }
        
        if(!Account::create($reg)){
            return jsend(500, null, 'Registration failed');
        }
        
        return jsend(200, ['userID' => $userID, 'deviceID' => $deviceID, 'accessToken' => $accessToken], 'Success');
    }
    
    
    public function emailLogin(){
		$email = Request::input('email');
		$password = md5(Request::input('password'));
		$deviceType = Request::input('deviceType');
		$deviceID = Request::input('deviceID', '');
		$language = Request::input('language');  //optional
        $latitude = ($v = Request::input('latitude'))===null ? null : (float)$v;   //optional
        $longitude = ($v = Request::input('longitude'))===null ? null : (float)$v; //optional
        $version = ($v = Request::input('version')) ? htmlspecialchars(substr($v, 0, 20), ENT_QUOTES) : NULL; //optional
		$accessToken = Generate::token();

        if(!$deviceType || $deviceID != htmlspecialchars($deviceID, ENT_QUOTES)){
            return jsend(403, null, 'Unkown device type, or wrong device ID');
        }
        
        $user = Account::authLoad($email, $password);
        if(!$user || !$user['userID']){
            return jsend(403, null, 'Login is failed');
        }
        
        if($user['isBlocked']){
            return jsend(403, null, 'Account is blocked');
        }
        $language = $language ? Locale::fixLanguage($language) : $user['language'];
        // 更新账户信息
        if(!Account::authUpdate($user['userID'], $language, $latitude, $longitude)){
            return jsend(500, null, 'Failture');
        }
        // 注册登录设备
        if(!Device::register($user['userID'], $accessToken, $deviceID, $deviceType, $language, $version, Request::getClientIp())){
            return jsend(500, null, 'Failture');
        }
        $user['accessToken'] = $accessToken;
        return jsend(200, User::format($user), 'Success');
    }
    
    
    public function refreshLogin(){
		$userID = Request::input('userID');
		$deviceID = Request::input('deviceID');
		$language = Locale::fixLanguage(Request::input('language'));
        $countryCode = '';
        
        if(!Account::isAvailable($userID, 0)){
            return jsend(500, null, 'Failture');
        }
        
        if(!Account::authRefresh($userID, Locale::isoToCountry($countryCode), $language) 
                || !Device::renewDevice($userID, $deviceID))
        {
            return jsend(500, null, 'Failture');
        }
        
        return jsend(200, null, 'Success');
    }
    
    
    public function forgetPassword(){
        
    }
    
    
    
    public function mobileVerify(){
        
        
    }
    
    
    public function isGuestLoginAvailable(){
		$ver = Request::input('version');

		if($ver==="1.1") {
            return jsend(200, null, 'True');
		} else {
            return jsend(500, null, 'False');
		}
	}
    
}
