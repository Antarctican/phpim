<?php namespace App\Http\Controllers\v1;
/**
 * Follows
 * @author d.x.c
 * @Created On Oct 10, 2015,2:11:00 PM
 */
use Config\Env;
use Request;
use App\Models\User\User;
use App\Models\Follow;

class FollowController extends \Laravel\Lumen\Routing\Controller{
    
    public function followToMeUserList(){
        $userID = Request::input('userID');
        $beforeTime = (int)Request::input('beforeTime');
		$count = (int)Request::input('count', 10);
                
        if(!$userID || !($uid = User::getUid($userID))){
            jsend(500, null, "We don't recognize you!");
        }
        
        $users = Follow::getFollowersOfUser($uid, $beforeTime, $count);
        
        jsend(200, User::formatMultiple($users));
    }
    
    public function followToUserList(){
        $userID = Request::input('userID');
        $beforeTime = (int)Request::input('beforeTime');
		$count = (int)Request::input('count', 10);
                
        if(!$userID || !($uid = User::getUid($userID))){
            jsend(500, null, "We don't recognize you!");
        }
        
        $users = Follow::getMyFollowingUsers($uid, $beforeTime, $count, false);
        
        jsend(200, User::formatMultiple($users));
    }
    
    public function followMutualUsers(){
        $userID = Request::input('userID');
        $beforeTime = (int)Request::input('beforeTime');
		$count = (int)Request::input('count', 10);
        
        if(!$userID || !($uid = User::getUid($userID))){
            jsend(500, null, "We don't recognize you!");
        }
        
        $users = Follow::getMutualFollowUsers($uid, $beforeTime, $count);
        
        jsend(200, User::formatMultiple($users));
    }
    
    
    public function followIsOk(){
        $userID = Request::input('userID');
        $toUserID = Request::input('targetUserID');
        
        if($userID == $toUserID || !($fromUid = User::getUid($userID)) || !($toUid = User::getUid($toUserID))){
            jsend(500, null, 'Failure');
        }
        
        $count = Follow::isFollowExists($fromUid, $toUid);
        
        jsend(200, ['result' => $count]);
    }
    
    
    public function followAction(){
        $userID = Request::input('userID');
        $toUserID = Request::input('targetUserID');
        
        if($userID == $toUserID || !($fromUid = User::getUid($userID)) || !($toUid = User::getUid($toUserID))){
            jsend(500, null, 'Failure');
        }
        
        User::updateLoginTime($fromUid, $userID);
        if(User::isInUserBlacklist($fromUid, $toUid)){
            jsend(403, null, 'You are banned by this user.');
        }
        
        $status = Follow::make($fromUid, $toUid);
        if($status['done']){
            // 不更新官方账号的粉丝数量及勋章
            if($toUserID != Env::OFFICIAL_USER_ID){
                Follow::updateNumAndBadge($toUid, true, $status['isMutual']);
            }
            jsend(200, null, 'Success');
        }
        else{
            jsend(500, null, 'Failure');
        }
        
    }
    
    
    public function followRemoveAction(){
        $userID = Request::input('userID');
        $toUserID = Request::input('targetUserID');
        
        if($userID == $toUserID || !($fromUid = User::getUid($userID)) || !($toUid = User::getUid($toUserID))){
            jsend(500, null, 'Failure');
        }
        
        if(Follow::delete($fromUid, $toUid)){
            Follow::updateNumAndBadge($toUid, false, false);
            jsend(200, null, 'Success');
        }
        else{
            jsend(500, null, 'Failure');
        }
        
    }
    
}
