<?php namespace App\Http\Controllers\v1;
/**
 * Post短文控制器
 * @author d.x.c
 * @Created On Oct 10, 2015,2:11:00 PM
 */

use Request;
use Config\Env;
use App\Models\User\User;
use App\Models\Post;
use App\Libraries\Generate;
use App\Libraries\Chars;
use App\Libraries\GeoipDb;

class PostController{
    
    public function load($mode = null){
		$userID = Request::input('userID');
        $targetUserID = Request::input('targetUserID'); // optional,取得someone的post时需要
		$mode = $mode ?: Request::input('mode');
		// $latitude = (float)Request::input('latitude');
		// $longitude = (float)Request::input('longitude');
		// $range = (float)Request::input('range');
        $gender = ($g = Request::input('gender')) ? User::fixGender($g) : null;  // optional
		$sameCity = Request::input('sameCity', 0); // optional,同城
		$beforeTime = (int)Request::input('beforeTime', 0);
		$count = (int)Request::input('count', 10);
        
        if(!$userID || !($uid = User::getUid($userID))){
            return jsend(500, null, 'Failure');
        }
        
        $postsAndUsers = $viewedPids = [];
        switch($mode){
            case 'self':
                if(!($user = User::get($uid)) || !($user = User::format($user)))
                {
                    return jsend(500, null, 'Failure');
                }
                $posts = Post::getUserPosts($uid, true, $beforeTime, $count);
                foreach($posts as $po){
                    $postsAndUsers[] = array_merge($user, Post::format($po));
                }
                break;
            
            case 'someone':
                if(!($uid = User::getUid($targetUserID))
                        || !($user = User::get($uid))
                        || !($user = User::format($user)))
                {
                    return jsend(500, null, 'Failure');
                }
                
                $posts = Post::getUserPosts($uid, false, $beforeTime, $count);
                foreach($posts as $po){
                    $viewedPids[] = $po['id'];
                    $postsAndUsers[] = array_merge($user, Post::format($po));
                }
                if($targetUserID == $userID){
                    $viewedPids = [];
                }
                break;
            
            case 'interested':
                $posts = Post::getFollowingUserPosts($uid, $beforeTime, $count);
                $authorUids = array_map(function($row){ return $row['authorUid']; }, $posts);
                $users = User::getUsers($authorUids);
                $usersSorted = [];
                foreach($users as $user){
                    $usersSorted[$user['uid']] = User::format($user);
                }
                unset($users);
                foreach($posts as $po){
                    if(!isset($usersSorted[$po['authorUid']])){
                        continue;
                    }
                    $viewedPids[] = $po['id'];
                    $postsAndUsers[] = array_merge($usersSorted[$po['authorUid']], Post::format($po));
                }
                break;
            
            case 'hot':
                $cityGeonameId = 0;
                if($sameCity){
                    $geo = GeoipDb::query( Request::getClientIp() );
                    $cityGeonameId = $geo['city_geoname_id'];
                }
                $postsAndUsers = Post::getHotPosts($beforeTime, $count, $gender, $cityGeonameId, true);
                $postsAndUsers = User::formatMultiple($postsAndUsers);
                foreach($postsAndUsers as &$row){
                    $viewedPids[] = $row['id'];
                    $row = Post::format($row);
                }
                break;
            
            default:
                $postsAndUsers = [];
                break;
        }
        if($viewedPids){
            Post::updateCount($viewedPids, null, 1, 0, 0);
        }
        
        return jsend(200, $postsAndUsers);
    }
    
    public function loadByUser(){
        return $this->load('someone');
    }
    
    public function publishAction(\Illuminate\Http\Request $req){
		$userID = $req->input('userID');
		$content = htmlspecialchars($req->input('message'), ENT_QUOTES);
		$picture = $req->input('picture');
		$postID = Generate::textID();
		$isHidden = $picture ? 0 : 1;
        
        if(!$userID || !($user = User::getByUserID($userID)) || !($uid = User::getUid($userID))){
            return jsend(500, null, 'Failure');
        }
        // 拒绝过快发post
        if(!Post::isFrequencyAllow($uid, Env::POST_FREQUNCY_SEC, Env::POST_FREQUNCY_NUM)){
            return jsend(403, null, 'I am tired');
        }
        
        if(!Env::IS_FREE && $user['gender'] == 'male' && $user['vipExpireAt']<time() && $user['isCelebrity'] == 0){
            $isHidden = 1;
        }
        // post 并不直接隐藏掉非法内容
        $isNeedReview = !Chars::inspect($content, Chars::INSPECT_MESSAGE, $user['language']);
        $ipv4 = Request::getClientIp();
        $geo = GeoipDb::query($ipv4, $user['language']);
        $id = Post::publish($postID, $content, $picture, $uid, $user['language'], $isHidden, $isNeedReview, $ipv4, $geo['city_geoname_id'], $geo['city_locale'], $geo['country_locale']);
        
        if($id){
            return jsend(200, ['postID' => $postID, 'postNumId' => $id], 'Success');
        }
        else{
            return jsend(500, null, 'Failure');
        }
    }
    
    public function deleteAction(){
		$userID = Request::input('userID');
		$postID = Request::input('postID');
        if(!$userID || !($uid = User::getUid($userID))){
            return jsend(500, null, 'Failure');
        }
        
        if(Post::delete($uid, $postID)){
            \App\Models\PostComment::delete($uid, $postID);
            // postNews未处理
            return jsend(200, null, 'Success');
        }
        else{
            return jsend(500, null, 'Failure');
        }
    }
    
    public function likeAction(){
		$userID = Request::input('userID');
		$postID = Request::input('postID');
        if(!$postID || !$userID || !($uid = User::getUid($userID))){
            return jsend(500, null, 'Failure');
        }
        
        if(Post::like($postID, $uid)){
            $authorUid = Post::getAuthorUid($postID, $uid);
            if($authorUid != $uid){
                Post::addPostNews(Post::POST_NEWS_TYPE, $authorUid, $uid, $postID, null, '');
                Post::updateAuthorBadage($authorUid, 1, 1);
            }
            Post::updateLikeCount($postID);
            return jsend(200, null, 'Success');
        }
        else{
            return jsend(500, null, 'Failure');
        }
        
    }
    
    
}
