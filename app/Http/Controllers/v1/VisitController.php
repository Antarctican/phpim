<?php namespace App\Http\Controllers\v1;
/**
 * 用户之间的访问
 * @author d.x.c
 * @Created On Oct 10, 2015,2:11:00 PM
 */

use Request;
use Config\Env;
use App\Models\User\User;
use App\Models\Visit;

class VisitController extends \Laravel\Lumen\Routing\Controller{
    
    public function visitAction(){
        $visitorID = Request::input('userID');
		$visitToID = Request::input('visitedUserID');
        if($visitorID == $visitToID || $visitToID == Env::OFFICIAL_USER_ID){
            jsend(200, null, '');
        }
        
        if(!($fromUid = User::getUid($visitorID)) || !($toUid = User::getUid($visitToID)))
        {
            jsend(500, null, 'Failure');
        }
        User::updateLoginTime($fromUid, $visitorID);
        
        /* 检查 我 是否在 被访问者 的黑名单中 */
        if(User::isInUserBlacklist($fromUid, $toUid)){
            jsend(500, null, 'You are banned by this user.');
        }
        
        $time = time();
        $visiting = Visit::getVisitation($fromUid, $toUid);
        if(!isset($visiting['createdAt'])){
            Visit::insert($fromUid, $toUid);
            Visit::visitUpdate($toUid);
        }
        elseif($visiting['createdAt'] < $time-$time%86400-date('Z')){
            Visit::update($fromUid, $toUid);
            Visit::visitUpdate($toUid);
        }
        
        jsend(200, null, 'Success');
    }
    
    public function getVisitorList(){
        $userID = Request::input('userID');
        $count = (int)Request::input('count');
        $beforeTime = (int)Request::input('beforeTime');
        
        if(!$userID || !($uid = User::getUid($userID))){
            jsend(500, null, 'Who are u?');
        }
        $visitors = Visit::getVisitors($uid, $count, $beforeTime);
        
        jsend(200, User::formatMultiple($visitors));
    }
    
}
