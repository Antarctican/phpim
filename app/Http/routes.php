<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$app->get('/', function(){ return 'Java is the best language in the earth!'; });

// register related
$app->post('v1/accountRegisterAction', 'v1\AccountController@register'); 
$app->post('v1/accountEmailLoginAction', 'v1\AccountController@emailLogin'); 
$app->post('v1/accountRetrievePasswordAction', 'v1\\'); //..
$app->post('v1/accountChangePasswordAction', 'v1\\'); //..
$app->post('v1/accountRefreshLoginAction', 'v1\AccountController@refreshLogin');
$app->get('v1/accountRefreshLoginAction', 'v1\AccountController@refreshLogin');
$app->post('v1/accountFbLoginAction', 'v1\AccountController@facebookLogin');
$app->get('v1/accountGuestLoginAvailable', 'v1\AccountController@isGuestLoginAvailable');

// user related
$app->get('v1/userNearby', 'v1\UserController@nearby');
$app->get('v1/userNearbyBid', 'v1\UserController@nearbyBid');
$app->get('v1/userGetByName', 'v1\UserController@getByName');
$app->get('v1/userDailyPopular', 'v1\UserController@dailyPopular');
$app->get('v1/userGlobalPopular', 'v1\UserController@globalPopular');
$app->get('v1/userRank', 'v1\UserController@rank');
$app->post('v1/userBidAction', 'v1\\');
$app->post('v1/userLoadProfile', 'v1\UserController@getProfile');
$app->get('v1/userLoadMyProfile', 'v1\UserController@getMyProfile');
$app->get('v1/userLoadAlbum', 'v1\UserController@album');
$app->get('v1/userCirculationValue', 'v1\UserController@circulationValue'); 
$app->get('v1/userFollowedUserIDs', 'v1\UserController@followedUserIDs');
$app->get('v1/userBlocked', 'v1\UserController@getBlacklist');
$app->post('v1/userBlockAction', 'v1\UserController@addToBlacklist');
$app->post('v1/userUnblockAction', 'v1\UserController@deleteFromBlacklist');
$app->post('v1/userReportAction', 'v1\UserController@userReportAction');
$app->post('v1/userUpdateAction', 'v1\UserController@update');
$app->post('v1/userPhotoAcceptAction', 'v1\UserController@photoAcceptAction');
$app->post('v1/userPhotoRejectAction', 'v1\UserController@photoRejectAction');
/* 这个接口是否去掉 */
$app->get('v1/userIsInTaiwan', 'v1\\');
$app->post('v1/userIsInTaiwan', 'v1\\');
$app->get('v1/userAppDriverCallback', 'v1\UserController@appDriverCallback');
$app->get('v1/userTapjoyCallback', 'v1\UserController@tapjoyCallback');
$app->post('v1/userAddPoint', 'v1\UserController@addPoint');
$app->post('v1/userAddSvip', 'v1\UserController@addSvip');
$app->get('v1/userGetOfficialAccounts', 'v1\UserController@getOfficialAccounts');
$app->get('v1/userGetCountry', 'v1\UserController@getCountry');
$app->post('v1/userUploadMedia', 'v1\UserController@uploadMedia');

// group related
$app->get('v1/groupsLoad', 'v1\\');
$app->get('v1/groupsLoadByCategory', 'v1\\');
$app->get('v1/groupsJoinedList', 'v1\\');
$app->get('v1/groupsSearch', 'v1\\');
$app->get('v1/groupMembersLoad', 'v1\\');
$app->post('v1/groupCreate', 'v1\\');
$app->post('v1/groupJoin', 'v1\\');
$app->post('v1/groupExit', 'v1\\');
$app->post('v1/groupPass', 'v1\\');
$app->post('v1/groupReject', 'v1\\');

	// message related
$app->get('v1/messageLoad', 'v1\\');
$app->post('v1/messageDeleteChatAction', 'v1\\');
$app->post('v1/messageDeleteAllChatAction', 'v1\\');
$app->post('v1/messageReadAction', 'v1\\');
$app->post('v1/messageReadAllAction', 'v1\\');
$app->post('v1/messageSendAction', 'v1\\');
$app->post('v1/messageSendBroadcastAction', 'v1\\');
$app->post('v1/messageRoamToMan', 'v1\\');

	// interest related
$app->get('v1/followToMeUserList', 'v1\FollowController@followToMeUserList');
$app->get('v1/followToUserList', 'v1\FollowController@followToUserList');
$app->get('v1/followMutualUsers', 'v1\FollowController@followMutualUsers');
$app->get('v1/followIsOk', 'v1\FollowController@followIsOk');
$app->post('v1/followAction', 'v1\FollowController@followAction');
$app->post('v1/followRemoveAction', 'v1\FollowController@followRemoveAction');

	// visit related
$app->post('v1/visitRecordAction', 'v1\VisitController@visitAction'); // 是否要去掉，在用户资料页直接更新访问
$app->get('v1/visitorList', 'v1\VisitController@getVisitorList');

	// post related
$app->get('v1/postLoad', 'v1\PostController@load');
$app->get('v1/postOfUserLoad', 'v1\PostController@loadByUser');
$app->get('v1/postNewsLoad', 'v1\PostController@');
$app->post('v1/postPublishAction', 'v1\PostController@publishAction');
$app->post('v1/postDeleteAction', 'v1\PostController@deleteAction');
$app->post('v1/postLikeAction', 'v1\PostController@likeAction');
//$app->post('v1/postLikeCancelAction', 'v1\PostController@likeCancelAction'); 这个接口可能需要
$app->get('v1/postLikeLoad', 'v1\\');
$app->post('v1/postReportAction', 'v1\\');
$app->get('v1/postCommentsLoad', 'v1\\');
$app->post('v1/postCommentPublishAction', 'v1\\');
$app->post('v1/postCommentReportAction', 'v1\\');

	// gift related
$app->post('v1/giftSendAction', 'v1\\');
$app->get('v1/giftReceivedList', 'v1\\');
$app->get('v1/giftLoadList', 'v1\\');
$app->get('v1/giftLoadList2', 'v1\\');

	// prize related
$app->get('v1/prizeListLoad', 'v1\\');
$app->get('v1/prizeGainedList', 'v1\\');
$app->post('v1/priceGainAction', 'v1\\');

	// payment related
$app->get('v1/paymentIsAvailable', 'v1\\');
$app->post('v1/paymentVerifyIOSReceipt', 'v1\\');
$app->post('v1/paymentVerifyAndroid', 'v1\\');

$app->get('v1/paymentBilling', 'v1\\');
$app->post('v1/paymentNoCreditCallback', 'v1\\');
$app->post('v1/paymentCreditCardCallback', 'v1\\');
$app->post('v1/paymentCreditCardResult', 'v1\\');
$app->post('v1/paymentPurchaseAction', 'v1\\');
$app->get('v1/paymentGetSubscription', 'v1\\');
$app->post('v1/paymentCancelSubscription', 'v1\\');

$app->get('v1/termOfUse', 'v1\\');

$app->get('v1/viewInStore', function() {
	// detect iOS and Android devices => redirect to AppStore
    $OS = null;
    if( preg_match('v1/(Android|iPhone|iPad|iWatch|iPod)/i',$_SERVER['HTTP_USER_AGENT'], $match) ){
        $OS = strtolower($match[1]);
    }
    
    switch($OS){
        case 'android':
			$downUrl = "market://details?id=com.machipopo.machi2";
            break;
        case 'iphone':
        case 'ipad':
        case 'iwatch':
        case 'ipod':
			$downUrl = "itms-apps://itunes.apple.com/app/id948161836";
            break;
        default:
            $downUrl = 'https://itunes.apple.com/app/id948161836';
            break;
    }
	header( "Location: $downUrl" );
    exit;
});

// 自动检查违纪情况
/*
$app->get('v1/patroller', function(){
    
});
 */

$app->get('test', function(\Illuminate\Http\Request $req){
    
        echo Generate::uuid();
        echo '<br>';
        echo Generate::token();
        echo '<br>';
        echo Generate::textID();
});