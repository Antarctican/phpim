<?php namespace App\Http\Middleware;
/**
 * API
 * @author d.x.c
 * @Created On Sep 26, 2015,10:46:43 AM
 */


use Closure;
use DB;

class ApiMiddleware {
    
    
    
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        DB::setFetchMode(2);
        if(strpos($_SERVER['HTTP_USER_AGENT'], 'Machiapp') !== 0){
            //return response('{"code":404}', 404);
        }
        
        return $next($request);
    }
    
    
    
}
