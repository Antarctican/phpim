<?php namespace App\Models\User;

/**
 * 
 * @author d.x.c
 * @Created On Sep 26, 2015,3:56:03 PM
 */

use DB;
use Cache;
use Config\Env;

class User {
    
    const USERID_TTL    = 86400;
    const USERID_PRE    = 'USER-';
    
    /**
     * 取得user表的主键id
     * @param type $userID
     * @return type
     */
    public static function getUid($userID){
        return Cache::rememberForever(self::USERID_PRE.$userID, function() use($userID){
            return DB::table('zx_user')->where('userID', '=', $userID)->pluck('uid');
        });
    }
    
    public static function get($uid){
        return DB::table('zx_user')->where('uid', '=', $uid)->first();
    }
    
    public static function getByUserID($userID){
        return DB::table('zx_user')->where('userID', '=', $userID)->first();
    }
    
    
    public static function getUsers(array $uids = [], array $orUserIDs = []){
        $users = DB::table('zx_user');
        if($uids){
            $users->whereIn('uid', $uids);
        }
        elseif($orUserIDs){
            $users->whereIn('userID', $orUserIDs);
        }
        else{
            return [];
        }
        $users = $users->get();
        
        return self::formatMultiple($users);
    }
    
    
    public static function getAlbum($uid = null, $orUserID = null){
        $query = DB::table('zx_user');
        if($uid){
            $query->where('uid', '=', $uid);
        }
        elseif($orUserID){
            $query->where('userID', '=', $orUserID);
        }
        else{
            return [];
        }
        $album = $query->take(1)->pluck('album');
        return $album && is_array($album = json_decode($album, true)) ? $album : [];
    }
    
    /**
     * 取得用户直接的价值流通值
     * @param type $fromUid
     * @param type $toUid
     * @return type
     */
    public static function getCirculationValue($fromUid, $toUid){
        return DB::table('zx_circulation_value')
                ->where('fromUid', '=', $fromUid)
                ->where('toUid', '=', $toUid)
                ->pluck('value');
    }
    
    /**
     * 取得用户所关注的人的ID集合
     * @param string $userID
     * @return array
     */
    public static function getUserIDsFollowedTo($userID){
        return DB::table('zx_follow')
                    ->where('fromUserID', '=', $userID)
                    ->lists('toUserID');
    }
    
    
    public static function update($userID, array $properties){
        $properties['age'] = DB::raw('(YEAR(CURDATE())-YEAR(birthday)-(RIGHT(CURDATE(), 5) <= RIGHT(birthday,5)))');
        return DB::table('zx_user')
                ->where('userID', '=', $userID)
                ->update($properties);
    }
    
    public static function updateLoginTime($uid, $orUserID = null, $delay = 10800){
        $query = DB::table('zx_user');
        if($uid){
            $query->where('uid', '=', $uid);
        }
        elseif($orUserID){
            $query->where('userID', '=', $orUserID);
        }
        else{
            return false;
        }
        $query->where('loginTime', '<', $delay)->update(['loginTime' => time()]);
        return true;
    }
    
    public static function isBlocked($uid, $orUserID = null){
        $user = DB::table('zx_user');
        if($uid){
            $user->where('uid', '=', $uid);
        }
        elseif($orUserID){
            $user->where('userID', '=', $orUserID);
        }
        else{
            return true;
        }
        return (bool)$user->where('isBlocked', '=', 1)->count();
    }
    
    /**
     * 用户是否在另一用户的黑名单中
     * @param type $checkUid
     * @param type $masterUid
     * @return type
     */
    public static function isInUserBlacklist($checkUid, $masterUid){
        return (bool)DB::table('zx_user_blacklist')
                    ->where('uid', '=', $masterUid)
                    ->where('banUid', '=', $checkUid)
                    ->count();
    }
    
    public static function getBlackList($uid, $length){
        $result = DB::table('user_blacklist AS bl')
                ->leftJoin('zx_user', 'user.uid', '=', 'bl.banUid')
                ->where('bl.uid', '=', $uid)
                ->orderBy('bl.createdAt', 'desc')
                ->take($length)
                ->get(['user.*', 'bl.id AS blackId']);
        /* 如果黑名单中有不存在的用户， 删除这条屏蔽记录 */
        $delBlackIds = [];
        foreach($result as $k => $row){
            if(!$row['uid'] && !$row['userID']){
                $delBlackIds[] = $row['blackId'];
                unset($result[$k]);
            }
        }
        if(count($delBlackIds)){
            DB::table('zx_user_blacklist')->whereIn('id', $delBlackIds)->delete();
        }
        return $result;
    }
    
    public static function addToBlacklist($uid, $banUid){
        $count = DB::table('zx_user_blacklist')
                    ->where('uid', '=', $uid)
                    ->where('banUid', '=', $banUid)
                    ->count();
        $done = true;
        if($count <= 0){
            $done = (bool)DB::table('zx_user_blacklist')->insert(['uid' => $uid, 'banUid' => $banUid, 'createdAt' => time()]);
        }
        
        \App\Models\Message\Message::delete($uid, $banUid);
        
        \App\Models\Visit::deleteBetween($uid, $banUid);
        
        \App\Models\Follow::deleteBetween($uid, $banUid);
        
        return $done;
    }
    
    public static function delFromBlacklist($uid, $banUid){
        DB::table('zx_user_blacklist')
                    ->where('uid', '=', $uid)
                    ->where('banUid', '=', $banUid)
                    ->delete();
        return true;
    }
    
    public static function givePoint($userID, $point = 0){
        return DB::table('zx_user')
                ->where('userID', '=', $userID)
                ->increment('point', (int)$point);
    }
    
    public static function giveVipAndPoint($userID, $points, $vipExtent, $svipExtent){
        $user = self::getByUserID($userID);
        if(empty($user)){
            return false;
        }
        
        $time = time();
        $values = ['point' => DB::raw('point'+$points)];
        if($vipExtent > 0 && $user['needGiveRenewVipBonus'] > $time){
            $vipExtent  += Env::RENEW_BONUS_VIP_EXTENT;
            $svipExtent += Env::RENEW_BONUS_SVIP_EXTENT;
            $values['needGiveRenewVipBonus'] = 0;
        }
        
        if($vipExtent > 0){
            $values['vipExpireAt'] = max($user['vipExpireAt'], $time) + $vipExtent;
        }
        
        if($svipExtent > 0){
            $values['vipPlatinumExpireAt'] = max($user['vipPlatinumExpireAt'], $time) + $svipExtent;
        }
        
        return self::update($userID, $values);
    }
    
    
    /* ============================= helper ================================= */
    
    /**
     * 辅助方法: 修正输入的gender
     * @param type $gender
     * @return type
     */
    public static function fixGender($gender){
        return in_array($gender, ['male', 'female']) ? $gender : '';
    }
    
    public static function fixConstellation($name){
        $all = ['aquarius', 'pisces', 'aries', 'taurus', 'gemini', 'cancer', 'leo', 'virgo', 'libra', 'scorpio', 'sagittarius', 'capricorn'];
        return in_array($name, $all) ? $name : '';
    }
    
    /**
     * 辅助方法： 格式化整行user数据
     * @param arrat $user       用户数据行
     * @param bool  $isMyself   是否是我自己
     * @return array
     */
    public static function format($user, $isMyself = false){
        if(!$user || !is_array($user)){
            return [];
        }
        
        $user['isOfficial'] = ($user['userID'] == Env::OFFICIAL_USER_ID);
        if($user['album']){
            $user['album'] = ($v = json_decode($user['album'], true)) ? $v : [];
        }
        if(!$user['avatar']){
            if($user['gender'] == 'male'){
                $user['avatar'] = '';
            }
            elseif($user['gender'] == 'female'){
                $user['avatar'] = '';
            }
            else{
                $user['avatar'] = '';
            }
        }
        unset($user['email']);
        unset($user['password']);
        unset($user['visitCountHistory']);
        if(!$isMyself){
            unset($user['token']);
            unset($user['accessToken']);
            unset($user['deviceID']);
        }

        $format = [];
        foreach($user as $col => $value){
            $format[$col] = $value;
        }
        return $format;
    }
    
    
    public static function formatMultiple($users, $excludeUserIDs = []){
        $result = [];
        foreach($users as $user){
            if($excludeUserIDs && isset($user['userID']) && in_array($user['userID'], $excludeUserIDs)){
                continue;
            }
            $result[] = self::format($user);
        }
        return $result;
    }
    
}
