<?php namespace App\Models\User;

/**
 * 
 * @author d.x.c
 * @Created On Sep 26, 2015,3:56:03 PM
 */
use DB;
use Config\Env;

class Search {
    
    const POPULAR_TOTAL     = 'visitCount';
    const POPULAR_DAILY     = 'visitTodayCount';
    const POPULAR_WEEKLY    = 'visit7daysCount';
    const POPULAR_MONTHLY   = 'visit30daysCount';
    
    public static function getNearby($length, $latitude, $longitude, $range = 3, $gender = null, $ageMin = 0, $ageMax = 150, $onlyNew = false){
        $query = DB::table('zx_user')
                    ->where('latitude', '>=', $latitude-$range)
                    ->where('latitude', '<=', $latitude+$range)
                    ->where('longitude', '>=', $longitude-$range)
                    ->where('longitude', '<=', $longitude+$range);
        if($gender){
            $query->where('gender', '=', $gender);
        }
        if($onlyNew){
            $query->where('registerTime', '>', time()-7*86400);
        }
        return $query->where('age', '>=', (int)$ageMin)
                    ->where('age', '<=', (int)$ageMax)
                    ->where('isHidden', '<>', 1)
                    ->where('isPhotoChecked', '=', 1)
                    ->orderBy('loginTime', 'desc')
                    ->take((int)$length)
                    ->get();
    }
    
    
    public static function getNearbyBid($length, $country, $latitude, $longitude, $range = 5, $userID = null){
        $query = DB::table('zx_user');
        $query->where(function($q) use ($country, $latitude, $longitude, $range, $userID){
            $q->where(function($q) use($country, $latitude, $longitude, $range) {
                $q->where('latitude', '>', $latitude - $range)
                    ->where('latitude', '<', $latitude + $range)
                    ->where('longitude', '>', $longitude - $range)
                    ->where('longitude', '<', $longitude + $range)
                    ->where('country', '=', $country);
            });
            
            if($userID){
                $q->orWhere('userID', '=', $userID);
            }
        });
        return $query->where('bid', '>', 0)
                    ->where('bidTime', '>', time()-86400)
                    ->where('avatar', '<>', '')
                    ->where('isBlocked', '<>', 1)
                    ->orderBy('bid', 'desc')
                    ->orderBy('bidTime', 'desc')
                    ->take((int)$length)
                    ->get();
    }
    
    public static function searchName($nameStr, $length = 30){
        if(empty($nameStr)){
            return [];
        }
        $nameStr = preg_replace('/`\'\"_\-%\*\(\)\$\@\!\=\+\^\\\>\<\?/i', '_', $nameStr);
        $segs = explode(' ', trim($nameStr));
        $segs = array_slice($segs, 0, 3);
        
        $query = DB::table('zx_user');
        $query->where('nickname', 'LIKE', '%'. implode('%', $segs) .'%');
        $query->orWhere('nickname', 'LIKE', '%'.$segs[0].'%');
        if(isset($segs[1])){
            $query->orWhere('nickname', 'LIKE', '%'.$segs[1].'%');
        }
        
        return $query->take((int)$length)->get();
    }
    
    public static function getPopularUsers($column, $length, $minAge, $maxAge, $latitude, $longitude, $range = 10, $city = null, $genders = [], $constellation = null){
        $query = DB::table('zx_user');
        if($city){
            $query->where('city', '=', $city);
        }
        if($constellation){
            $query->where('constellation', '=', $constellation);
        }
        if($genders && is_array($genders)){
            $query->whereIn('gender', $genders);
        }
        if(is_float($latitude) && is_float($longitude)){        
            $query->where('latitude', '>', $latitude - $range)
                ->where('latitude', '<', $latitude + $range)
                ->where('longitude', '>', $longitude - $range)
                ->where('longitude', '<', $longitude + $range);
        }
        return $query->where('age', '>', $minAge)
                ->where('age', '<', $maxAge)
                ->where('isPhotoChecked', '=', 1)
                ->where('isHidden', '=', 0)
                ->where('isBlocked', '=', 0)
                ->where('userID', '<>', Env::OFFICIAL_USER_ID)
                ->orderBy($column, 'desc')
                ->take((int)$length)
                ->get();
    }
    
    
    public static function rank($scope, $length, $country, $province, $city, $duration = 604800){
        $query = DB::table('zx_user')->where('loginTime', '>', time()-$duration);
        if($city){
            $query->where('city', '=', $city);
        }
        if($province){
            $query->where('province', '=', $province);
        }
        if($country){
            $query->where('country', '=', $country);
        }
        
        if($scope == 'topMale'){
            $query->where('gender', '=', 'male');
        }
        elseif($scope == 'topFemale'){
            $query->where('gender', '=', 'female');
        }
        $query->where('avatar', '<>', '')
            ->where('isBlocked', '<>', 1)
            ->where('isPhotoChecked', '=', 1)
            ->where('userID', '<>', Env::OFFICIAL_USER_ID);
            
        switch($scope){
            case 'topMale':
            case 'topFemale':{
                $query->orderBy('visitCount', 'desc');
                break;
            }
            case 'topGiving':{ // topBaller
                $query->orderBy('sentGiftValue', 'desc');
                break;
            }
            case 'topEarnings':{ // mostMachi
                $query->orderBy('receivedGiftValue', 'desc');
                break;
            }
            case 'topFollowed':{ // topFollowed
                $query->orderBy('followerCount', 'desc');
                break;
            }
            default:
                return [];
        }
        return $query->take((int)$length)->get();
    }
    
    
    
    
}
