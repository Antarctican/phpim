<?php namespace App\Models;
/**
 * 设备管理Model
 * @author d.x.c
 * @Created On Oct 21, 2015,3:35:30 PM
 */

use DB;

class Device {
    // 最多允许的设备数量, >=1
    protected static $devicesAllowNum   = 5;

    public static function setDeviceAllowNum($num){
        self::$devicesAllowNum = max(1, $num);
    }

    /**
     * 注册一个设备。 超出允许的设备数量，会弹出最早的设备(登录失效)
     * @param type $userID
     * @param type $accessToken
     * @param type $deviceID
     * @param type $deviceType
     * @param type $version
     * @param type $ip
     * @return boolean
     */
    public static function register($userID, $accessToken, $deviceID, $deviceType, $lang, $version = null, $ip = null){
        if(!$userID || !$accessToken || !$deviceID){
            return false;
        }
        DB::transaction(function() use($userID, $accessToken, $deviceID, $deviceType, $lang, $version, $ip){
            $values = [
                'userID'    => $userID, 
                'accessToken'=> $accessToken,
                'deviceID'  => $deviceID, 
                'deviceType'=> $deviceType,
                'lang'      => $lang,
                'version'   => $version,
                'ip'        => $ip,
                'loginTime' => time()
            ];
            $updateCount = DB::table('zx_user_device')
                    ->where('userID', '=', $userID)
                    ->where('deviceID', '=', $deviceID)
                    ->update($values);
            if($updateCount<=0){
                DB::table('zx_user_device')->insert($values);
            }
            $ids = DB::table('zx_user_device')
                    ->where('userID', '=', $userID)
                    ->orderBy('loginTime', 'desc')
                    ->skip(self::$devicesAllowNum)
                    ->take(10)
                    ->lists('id');
            if(count($ids) > 0){
                DB::table('zx_user_device')->whereIn('id', $ids)->delete();
            }
            
        });
        return true;
    }
    
    
    public static function isDeviceLoggedIn($deviceID, $accessToken, $ttl = 86400){
        return (bool)DB::table('zx_user_device')
                        ->where('deviceID', '=', $deviceID)
                        ->where('accessToken', '=', $accessToken)
                        ->where('loginTime', '>', time()-$ttl);
    }
    
    /**
     * 更新登录设备的登录时间
     * @param type $userID
     * @param type $deviceID
     * @return type
     */
    public static function renewDevice($userID, $deviceID){
        return DB::table('zx_user_device')
                    ->where('userID', '=', $userID)
                    ->where('deviceID', '=', $deviceID)
                    ->update(['loginTime' => time()]);
    }
    
    public static function removeDevice($deviceID){
        return (bool)DB::table('zx_user_device')
                        ->where('deviceID', '=', $deviceID)
                        ->delete();
    }
    
}
