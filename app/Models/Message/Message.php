<?php namespace App\Models\Message;
/**
 * message处理model
 * @author d.x.c
 * @Created On Oct 22, 2015,5:29:35 PM
 */

use DB;

class Message {
    
    
    public static function delete($ownerUid, $receiverUid){
        return DB::delete("DELETE FROM message WHERE owner=? AND ((sender=? AND receiver=?) OR (sender=? AND receiver=?))", 
                [$ownerUid, $ownerUid, $receiverUid, $receiverUid, $ownerUid]);
        
    }
    
}
