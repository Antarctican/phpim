<?php namespace App\Models;
/**
 * 
 * @author d.x.c
 * @Created On Oct 22, 2015,7:41:18 PM
 */

use DB;
use Config\Env;

class Visit {
    
    public static function insert($fromUid, $toUid){
        if(!$fromUid || !$toUid){
            return false;
        }
        return DB::table('zx_visit')->insert(['fromUid' => $fromUid, 'toUid' => $toUid, 'createdAt' => time()]);
    }
    
    public static function update($fromUid, $toUid){
        return DB::table('zx_visit')
                ->where('fromUid', '=', $fromUid)
                ->where('toUid', '=', $toUid)
                ->update(['createdAt' => time()]);
    }
    
    /**
     * 
     * @param type $uid
     * @return type
     */
    public static function visitUpdate($uid){
        $val = DB::table('zx_user')
                    ->select('visitCount', 'visitTodayCount', 'visit7daysCount', 'visit30daysCount', 'visitCountHistory', 'badgeVisit')
                    ->where('uid', '=', $uid)
                    ->first();
        
        if(!$val['visitCountHistory'] || !($val['visitCountHistory'] = json_decode($val['visitCountHistory'], true))){
            $val['visitCountHistory'] = [];
        }
        
        $time = time();
        $today = date('Y-m-d');
        $_todayStart = $time-$time%86400-date('Z');
        $_7daysStart = date('Y-m-d', $time-86400*6);
        $_30daysStart = date('Y-m-d', $time-86400*29);
        
        $hsty = $val['visitCountHistory'];
        $hsty[$today] = DB::table('zx_visit')
                    ->where('toUid', '=', $uid)
                    ->where('createdAt', '>=', $_todayStart)
                    ->where('createdAt', '<', $_todayStart+86400)
                    ->count();
        $val['visitTodayCount']  = $hsty[$today];
        $val['visit7daysCount']  = 0;
        $val['visit30daysCount'] = 0;
        krsort($hsty, SORT_REGULAR);
        foreach($hsty as $date => $count){
            if($date <= $today && $date >= $_7daysStart){
                $val['visit7daysCount'] += $count;
            }
            if($date <= $today && $date >= $_30daysStart){
                $val['visit30daysCount'] += $count;
            }
            if($date < $_30daysStart) break;
        }
        $val['visitCount'] = max($val['visit30daysCount'], $val['visitCount']+1);
        $val['visitCountHistory'] = json_encode( array_slice($hsty, 0, 62) );
        $val['badgeVisit'] += 1;
        return DB::table('zx_user')->where('uid', '=', $uid)->update($val);
    }
    
    
    
    public static function getVisitors($uid, $length = 10, $beforeTime = 0){
        $query = DB::table('zx_user');
        if($beforeTime){
            $query->where('createdAt', '<', $beforeTime);
        }
        return $query->join('zx_visit', 'user.uid', '=', 'visit.fromUid')
                ->where('toUid', '=', $uid)
                ->orderBy('createdAt', 'desc')
                ->take((int)$length)
                ->get();
    }
    
    public static function getVisitation($fromUid, $toUid){
        return DB::table('zx_visit')
                    ->where('fromUid', '=', $fromUid)
                    ->where('toUid', '=', $toUid)
                    ->first();
    }
    
    public static function deleteBetween($uid1, $uid2){
        return DB::table('zx_visit')
                ->where(function($query) use($uid1, $uid2){
                    $query->where('fromUid', '=', $uid1)->where('toUid', '=', $uid2);
                })
                ->orWhere(function($query) use($uid1, $uid2){
                    $query->where('fromUid', '=', $uid2)->where('toUid', '=', $uid1);
                })
                ->delete();
    }
    
}
