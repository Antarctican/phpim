<?php namespace App\Models;
/**
 * 短文处理model
 * @author d.x.c
 * @Created On Nov 4, 2015,9:23:12 AM
 */

use DB;
use App\Models\Follow;

class Post {
    
    const POST_NEWS_TYPE        = 'like';
    
    /**
     * 取得用户的post列表。
     * @param int $uid
     * @param bool $isMyself    这个用户是我自己？——当以自己身份取得自己的posts时，不过滤
     * @param int $beforeTime
     * @param int $length
     * @param bool $withUser        联合查出用户数据
     * @return type
     */
    public static function getUserPosts($uid, $isMyself = false, $beforeTime = 0, $length = 10){
        $query = DB::table('zx_post')->where('authorUid', '=', $uid);
        if($beforeTime > 0){
            $query->where('createdAt', '<', $beforeTime);
        }
        if(!$isMyself){
            $query->where('isHidden', '=', 0);
        }
        
        return $query->orderBy('createdAt', 'desc')->take((int)$length)->get();
    }
    
    /**
     * 取得用户follow的其他用户的短文
     * @param int $uid
     * @param int $beforeTime
     * @param int $length
     * @param bool $withUser        联合查出用户数据
     * @return type
     */
    public static function getFollowingUserPosts($uid, $beforeTime = 0, $length = 10){
        $followingUids = Follow::getMyFollowingUserUids($uid, $beforeTime, $length, true);
        
        $query = DB::table('zx_post')->whereIn('authorUid', $followingUids);
        if($beforeTime > 0){
            $query->where('createdAt', '<', $beforeTime);
        }
        return $query->where('isHidden', '=', 0)
                    ->orderBy('createdAt', 'desc')
                    ->take((int)$length)
                    ->get();
    }
    
    /**
     * 取得热门短文
     * @param int $beforeTime
     * @param int $length
     * @param string $gender
     * @param string $country
     * @param string $province
     * @param string $city
     * @param bool $withUser        联合查出用户数据
     * @return type
     */
    public static function getHotPosts($beforeTime = 0, $length = 10, $gender = null, $cityGeonameId = 0, $withUser = false){
        $query = DB::table('post AS p')->join('user AS u', 'u.uid', '=', 'p.authorUid');
        $withUser ? $query->select('p.*', 'u.*', 'p.country AS country', 'p.city as city') : $query->select('p.*');
        if($cityGeonameId){
            $query->where('p.city_geoname_id', '=', $cityGeonameId);
        }
        if($beforeTime > 0){
            $query->where('p.createdAt', '<', $beforeTime);
        }
        if($gender){
            $query->where('u.gender', '=', $gender);
        }
        $query->where('p.isHidden', '=', 0)
            ->where('u.isHidden', '=', 0)
            ->where(function($q){
                $q->where('p.viewCount', '>=', 100)
                    ->orWhere('u.vipExpireAt', '>=', time())
                    ->orWhere('u.visitCount', '>=', 1000)
                    ->orWhere('u.isCelebrity', '=', 1);
            });
        return $query->orderBy('p.createdAt', 'desc')
                    ->take((int)$length)
                    ->get();
    }
   
    /**
     * 更新计数
     * @param int $ids
     * @param string $orPostIDs
     * @param int $viewCount       增加数
     * @param int $likeCount       增加数
     * @param int $commentCount    增加数
     * @return bool
     */
    public static function updateCount($ids, $orPostIDs, $viewCount = 0, $likeCount = 0, $commentCount = 0){
        $query = DB::table('zx_post');
        $ids ? $query->whereIn('id', $ids) : $query->whereIn('postID', $orPostIDs);
        
        return $query->update([
                'viewCount' => DB::raw('viewCount+'. intval($viewCount) ),
                'likeCount' => DB::raw('likeCount+'. intval($likeCount) ),
                'commentCount' => DB::raw('commentCount+'. intval($commentCount) ),
            ]);
    }
    
    /**
     * 检查post的发布频率是否允许
     * @param type $uid
     * @param type $inSeconds
     * @param type $num
     * @return type
     */
    public static function isFrequencyAllow($uid, $inSeconds = 0, $num = 10){
        $count = DB::table('zx_post')
                    ->where('authorUid', '=', $uid)
                    ->where('createdAt', '>=', time()-$inSeconds)
                    ->count();
        return $count < $num ? true : false;
    }
    
    public static function publish($postID, $content, $picture, $authorUid, $language, $isHidden = 0, $isNeedReview = 0, $ipv4 = null, $geonameId = 0, $city = null, $country = null){
        return DB::table('zx_post')->insertGetId([
                    'postID'          => $postID,
                    'content'         => $content,
                    'picture'         => $picture,
                    'authorUid'       => $authorUid,
                    'language'        => $language,
                    'isHidden'        => $isHidden ? 1 : 0,
                    'isNeedReview'    => $isNeedReview ? 1 : 0,
                    'ipv4'            => $ipv4,
                    'city_geoname_id' => $geonameId,
                    'city'            => $city,
                    'country'         => $country,
                    'createdAt'       => time(),
        ]);
    }
    
    public static function delete($uid, $postID){
        return DB::table('zx_post')
                    ->where('authorUid', '=', $uid)
                    ->where('postID', '=', $postID)
                    ->delete();
    }
    
    public static function like($postID, $uid){
        return DB::insert('REPLACE INTO post_like SET postID=?, uid=?, createdAt=?', [$uid, $postID, time()]);
    }
    
    public static function updateLikeCount($postID){
        return DB::update('update post set likeCount=(select count(*) from post_like where postID=?) where postID=?', [$postID, $postID]);
    }
    
    public static function updatePosCommentCount($postID){
        return DB::update('update post set commentCount=(select count(*) from post_comment where postID=?) where postID=?', [$postID, $postID]);
    }
    
    public static function updateAuthorBadage($uid, $badge = 1, $postNewsBadge = 1){
        return DB::table('zx_user')
                ->where('uid', '=', $uid)
                ->update(['badge' => DB::raw('badge+'.intval($badge)), 'badgePostNews' => DB::raw('badgePostNews+'.intval($postNewsBadge))]);
    }
    
    public static function getAuthorUid($postID){
        return DB::table('zx_post')->where('postID', '=', $postID)->pluck('authorUid');
    }
    
    public static function addPostNews($type, $uid, $fromUid, $postID = null, $commentID = null, $content = ''){
        $postID = $postID ?: null; // 重要
        $commentID = $commentID ?: null; // 重要
        
        return DB::insert('REPLACE INTO post_news set type=?, uid=?, fromUid=?, postID=?, commentID=?, content=?, createdAt=?',
                    [$type, $uid, $fromUid, $postID, $commentID, $content, time()]);
    }
    
    
    
    
    /*======================== helpers =======================================*/
    
    /**
     * 这个方法不能删掉$post里含有的user数据
     * @param type $post
     * @return type
     */
    public static function format($post){
        
        return $post;
    }
    
    
}
