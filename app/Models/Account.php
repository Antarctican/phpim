<?php namespace App\Models\Account;

/**
 * 账号Model
 * @author d.x.c
 * @Created On Sep 26, 2015,3:21:28 PM
 */

use DB;

class Account {
    
    public static function exists($email){
        return DB::table('zx_user')->where('email', '=', $email)->count();
    }
    
    public static function isAvailable($userID, $isBlocked = -1){
        $query = DB::table('zx_user')
                ->where('userID', '=', $userID);
        if($isBlocked >= 0){
            $query->where('isBlocked', '=', (int)$isBlocked);
        }
        return $query->count();
    }
    
    
    public static function create($reg){
        return DB::table('zx_user')->insertGetId($reg);
    }
    
    public static function authLoad($email, $password){
        return DB::table('zx_user')
                    ->where('email', '=', $email)
                    ->where('password', '=', $password)
                    ->first();
    }
    
    
    public static function authUpdate($userID, $language = 'en-US', $latitude = 0, $longitude = 0){
        $values = [
                'age'           => DB::raw('(YEAR(CURDATE())-YEAR(birthday)-(RIGHT(CURDATE(), 5) <= RIGHT(birthday,5)))'),
                'language'      => $language,
                'loginTime'     => time()
        ];
        if(is_float($latitude)){
            $values['latitude'] = $latitude;
        }
        if(is_float($longitude)){
            $values['longitude'] = $longitude;
        }
        return DB::table('zx_user')
                ->where('userID', '=', $userID)
                ->update($values);
    }
    
    
    public static function authRefresh($userID, $country = null, $language = null){
        return DB::table('zx_user')
                ->where('userID', '=', $userID)
                ->update(['country' => $country, 'language' => $language, 'loginTime' => time()]);
    }
    
    
}
