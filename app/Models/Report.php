<?php namespace App\Models;
/**
 * 举报用户操作model
 * @author d.x.c
 * @Created On Oct 23, 2015,8:38:58 PM
 */
 
use DB;

class Report {
    
    
    /**
     * 添加举报记录
     * @param int $informerUid      告密者uid
     * @param int $accusedUid       被告者uid
     * @param string $reason        原因
     * @param int $level            违规等级， 一般为1
     * @param int $postId           被举报的post id
     * @param int $commentId        被举报的评论id
     * @param int $isAdminAction    是否系统行为
     * @return boolean
     */
    public static function add($informerUid, $accusedUid, $reason = '', $level = 1, $postId = 0, $commentId = 0, $isAdminAction = 0){
        $count = DB::table('zx_report')
                    ->where('informerUid', '=', $informerUid)
                    ->where('accusedUid', '=', $accusedUid)
                    ->count();
        if($count){
            return true;
        }
        else{
            return DB::table('zx_report')->insert([
                'informerUid'   => $informerUid,
                'accusedUid'    => $accusedUid,
                'reason'        => $reason,
                'postId'        => $postId,
                'commentId'     => $commentId,
                'isSysAction'   => $isAdminAction,
                'level'         => $level,
                'createdAt'     => time()
            ]);
        }
    }
    
    /**
     * 检查用户被举报违规程度。 符合条件将锁定此账户
     * @param int $accusedUid       用户记录的主键id
     * @param int $limitTimes       达到此被举报次数，将锁定用户
     * @param int $orLimitLevel     达到此违规等级， 将锁定用户
     * @return boolean
     */
    public static function checkAndBlockUser($accusedUid, $limitTimes = 5, $orLimitLevel = 10){
        $status = DB::table('zx_report')
                    ->where('accusedUid', '=', $accusedUid)
                    ->get(DB::raw('count(*) as times, sum(level) as the_level'));
        if(empty($status)){
            return false;
        }
        if($status['times'] >= $limitTimes || $status['the_level'] >= $orLimitLevel){
            return (bool)DB::table('zx_user')
                    ->where('uid', '=', $accusedUid)
                    ->update(['isBlocked' => 1, 'isHidden' => 1]);
        }
        return false;
    }
    
}
