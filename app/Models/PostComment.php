<?php namespace App\Models;
/**
 * post评论相关
 * @author d.x.c
 * @Created On Nov 10, 2015,6:09:23 PM
 */
use DB;

class PostComment {
     
    public static function delete($uid, $postID){
        return DB::table('zx_post_comment')
                    ->where('authorUid', '=', $uid)
                    ->where('postID', '=', $postID)
                    ->delete();
    }
    
    
}
