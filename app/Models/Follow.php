<?php namespace App\Models;

/**
 * 关注model
 * 在Follow表设计上，aUid < bUid, a2b: aUid是否关注bUid, b2a: bUid是否关注aUid。 
 * 此特殊设计是为了减少数据库查询次数
 * @author d.x.c
 * @Created On Oct 22, 2015,6:58:28 PM
 */

use DB;

class Follow {
    /**
     * 取得两个用户之间的一行follow记录
     * @param int $uid1
     * @param int $uid2
     * @return type
     */
    public static function getBetween($uid1, $uid2){
        $aUid = min($uid1, $uid2);
        $bUid = max($uid1, $uid2);
        return DB::table('zx_follow')->where('aUid', '=', $aUid)->where('bUid', '=', $bUid)->first();
    }
    
    /**
     * 取得用户的关注者(粉丝)
     * @param int $uid
     * @param int $beforeTime      指定时间之前
     * @param int $length          条数
     * @return type
     */
    public static function getFollowersOfUser($uid, $beforeTime = 0, $length = 50){
        $uids = DB::table('zx_follow')
                    ->select(DB::raw('IF(aUid='.$uid.', bUid, aUid) as uid'), DB::raw('IF(aUid='.$uid.', b2aTime, a2bTime) as createdAt'))
                    ->where(function($q) use($uid){
                        $q->where('aUid', '=', $uid)->where('b2a', '=', 1);
                    })
                    ->orWhere(function($q) use($uid){
                        $q->where('bUid', '=', $uid)->where('a2b', '=', 1);
                    })
                    ->having('createdAt', '<', $beforeTime)
                    ->orderBy('createdAt', 'desc')
                    ->take((int)$length)
                    ->lists('uid');
        if(empty($uids)){
            return [];
        }
        return DB::table('zx_user')->whereIn('uid', $uids)->where('isBlocked', '<>', 1)->get();
    }
    
    /**
     * 取得某用户正在关注中的用户
     * @param int $uid
     * @param int $beforeTime  此时间之前
     * @param int $length      条数
     * @param bool $skipBlock  是否排除已冻结的用户
     * @return type
     */
    public static function getMyFollowingUsers($uid, $beforeTime = 0, $length = 50, $skipBlock = false){
        $uids = self::getMyFollowingUserUids($uid, $beforeTime, $length, false);
        if(is_array($uids) && $uids){
            $query = DB::table('zx_user')->whereIn('uid', $uids);
            if($skipBlock){
                $query->where('isBlocked', '<>', 1);
            }
            return $query->get();
        }
        return [];
    }
    
    /**
     * 获取我关注中的用户uid数组集合
     * @param int $uid         用户uid
     * @param int $beforeTime  从此时间之前查找
     * @param int $length      取得条数
     * @param bool $skipBlock  是否排除已冻结的用户
     * @return type
     */
    public static function getMyFollowingUserUids($uid, $beforeTime = 0, $length = 50, $skipBlock = false){
        $uids = DB::table('zx_follow')
                ->select(DB::raw('IF(aUid=' . $uid . ', bUid, aUid) as uid'), DB::raw('IF(aUid=' . $uid . ', b2aTime, a2bTime) as createdAt'))
                ->where(function($q) use($uid) {
                    $q->where('aUid', '=', $uid)->where('a2b', '=', 1);
                })
                ->orWhere(function($q) use($uid) {
                    $q->where('bUid', '=', $uid)->where('b2a', '=', 1);
                })
                ->having('createdAt', '<', $beforeTime)
                ->orderBy('createdAt', 'desc')
                ->take((int) $length)
                ->lists('uid');
        if($skipBlock && $uids){
            $uids = DB::table('zx_user')->whereIn('uid', $uids)->where('isBlocked', '<>', 1)->lists('uid');
        }
        return $uids;
    }
    
    /**
     * 取得follow中的并且是互相follow的用户
     * @param int $uid
     * @param int $beforeTime
     * @param int $length
     * @return type
     */
    public static function getMutualFollowUsers($uid, $beforeTime = 0, $length = 50){
        $uids = DB::table('zx_follow')
                    ->select(DB::raw('IF(aUid='.$uid.', bUid, aUid) as uid'), DB::raw('IF(aUid='.$uid.', b2aTime, a2bTime) as createdAt'))
                    ->where(function($q) use($uid){
                        $q->where('aUid', '=', $uid)->orWhere('bUid', '=', $uid);
                    })
                    ->where('a2b', '=', 1)
                    ->where('b2a', '=', 1)
                    ->having('createdAt', '<', $beforeTime)
                    ->orderBy('createdAt', 'desc')
                    ->take((int)$length)
                    ->lists('uid');
        if(empty($uids)){
            return [];
        }
        return DB::table('zx_user')->whereIn('uid', $uids)->where('isBlocked', '<>', 1)->get();
    }
    
    /**
     * 一用户是否follow了另一用户
     * @param type $fromUid
     * @param type $toUid
     * @return int  返回整数
     */
    public static function isFollowExists($fromUid, $toUid){
        $query = DB::table('zx_follow');
        if($fromUid < $toUid){
            $query->where('aUid', '=', $fromUid)->where('bUid', '=', $toUid)->where('a2b', '=', 1);
        }
        elseif($fromUid > $toUid){
            $query->where('aUid', '=', $toUid)->where('bUid', '=', $fromUid)->where('b2a', '=', 1);
        }
        return $query->count();
    }
    
    /**
     * 使用户关注另一用户
     * @param type $fromUid
     * @param type $toUid
     * @return boolean
     */
    public static function make($fromUid, $toUid){
        $result = ['done' => false, 'isMutual' => false];
        if($fromUid == $toUid){
            return $result;
        }
        $aUid = min($fromUid, $toUid);
        $bUid = max($fromUid, $toUid);
        
        $row = self::getBetween($fromUid, $toUid);
        $values = [];
        // 如果已存在相应记录，则视为follow失败
        if($fromUid < $toUid) {
            if($row['a2b'] == 1) { return $result; }
            $values['a2b'] = 1;
            $values['a2bTime'] = time();
        } else {
            if($row['b2a'] == 1) { return $result; }
            $values['b2a'] = 1;
            $values['b2aTime'] = time();
        }
        // 是否相互关注
        $result['isMutual'] = ($row && $row['a2b'] == 1 && $row['b2a'] == 1);
        
        if(empty($row) || !is_array($row)){
            $values['aUid'] = $aUid;
            $values['bUid'] = $bUid;
            $result['done'] = (bool)DB::table('zx_follow')->insert($values);
        }
        else{
            $result['done'] = (bool)DB::table('zx_follow')->where('id', '=', $row['id'])->update($values);
        }
        return $result;
    }
    
    /**
     * 删除用户1到用户2的follow
     * @param type $fromUid
     * @param type $toUid
     * @return boolean
     */
    public static function delete($fromUid, $toUid){
        if(!($row = self::getBetween($fromUid, $toUid))){
            return true;
        }
        
        if($fromUid < $toUid){
            $row['a2b'] = 0;
            $row['a2bTime'] = 0;
        }
        else{
            $row['b2a'] = 0;
            $row['b2aTime'] = 0;
        }
        
        if($row['a2b'] == 0 && $row['b2a'] == 0){
            return DB::table('zx_follow')->where('id', '=', $row['id'])->delete();
        }
        else{
            return DB::table('zx_follow')->where('id', '=', $row['id'])->update($row);
        }
    }
    
    /**
     * 删除两者之间的follow
     * @param int $uid1
     * @param int $uid2
     * @return type
     */
    public static function deleteBetween($uid1, $uid2){
        return DB::table('zx_follow')
                ->where(function($query) use($uid1, $uid2){
                    $query->where('aUid', '=', $uid1)->where('bUid', '=', $uid2);
                })
                ->orWhere(function($query) use($uid1, $uid2){
                    $query->where('aUid', '=', $uid2)->where('bUid', '=', $uid1);
                })
                ->delete();
    }
    
    
    /**
     * 更新用户的被关注次数及勋章
     * @param int $uid             
     * @param type $addFollowBadge  增加follow勋章
     * @param type $addMutualBadge  增加互相关注勋章
     * @return type
     */
    public static function updateNumAndBadge($uid, $addFollowBadge = false, $addMutualBadge = false){
        return DB::table('zx_user')
                ->where('uid', '=', $uid)
                ->update([
                    'followerCount'     => (int)self::countFollower($uid),
                    'badgeFollow'       => DB::raw('badgeFollow+'.($addFollowBadge?1:0)), 
                    'badgeMutualFollow' => DB::raw('badgeMutualFollow+'.($addMutualBadge?1:0)), 
                ]);
    }
    
    /**
     * 统计用户粉丝的次数
     * @param int $uid
     * @return type
     */
    public static function countFollower($uid){
        return DB::table('zx_follow')
                ->where(function($query) use($uid){
                    $query->where('aUid', '=', $uid)->where('b2a', '=', 1);
                })
                ->orWhere(function($query) use($uid){
                    $query->where('bUid', '=', $uid)->where('a2b', '=', 1);
                })
                ->count();
    }
    
}
