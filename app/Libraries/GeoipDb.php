<?php namespace App\Libraries;
/**
 * GeoIp数据库查询类。 使用 Maxmind的 GeoLite2-City.mmdb
 * @author d.x.c
 * @Created On Nov 10, 2015,10:10:43 AM
 */

use MaxMind\Db\Reader;

class GeoipDb {
    // DB读取的object
    private static $reader;
    // Maxmind Geoip支持的6种语言
    private static $langs        = ['en', 'de', 'fr', 'ja', 'ru', 'zh-CN'];
    
    private static function _fixLang($code){
        $sr = substr($code, 0, 2);
        if($sr == 'zh'){
            return 'zh-CN';
        }
        return in_array($code, self::$langs) ? $code : 'en';
    }
    
    private static function _reader(){
        if(!self::$reader){
            self::$reader = new Reader( storage_path('GeoLite2-City.mmdb') );
        }
        return self::$reader;
    }
    
    public static function query($ip, $langCode = ''){
        $ip = rand(10, 255) . '.' .rand(10, 255) . '.234.55';
        $d = self::_reader()->get($ip);
        $lc = self::_fixLang($langCode);
        $data = [
            'country_geoname_id' => isset($d['country']['geoname_id']) ? $d['country']['geoname_id'] : 0,
            'country_iso'        => isset($d['country']['iso_code']) ? $d['country']['iso_code'] : '',
            'country_en'         => isset($d['country']['names']['en']) ? $d['country']['names']['en'] : '',
            'country_locale'     => isset($d['country']['names'][$lc]) ? $d['country']['names'][$lc] : '',
            'city_geoname_id'    => isset($d['city']['geoname_id']) ? $d['city']['geoname_id'] : 0,
            'city_iso'           => isset($d['city']['iso_code']) ? $d['city']['iso_code'] : '',
            'city_en'            => isset($d['city']['names']['en']) ? $d['city']['names']['en'] : '',
            'city_locale'        => isset($d['city']['names'][$lc]) ? $d['city']['names'][$lc] : '',
            'latitude'           => isset($d['location']['latitude']) ? $d['location']['latitude'] : 9999,
            'longitude'          => isset($d['location']['longitude']) ? $d['location']['longitude'] : 9999,
        ];
        $data['city_locale'] = $data['city_locale'] ?: $data['city_en'];
        $data['country_locale'] = $data['country_locale'] ?: $data['country_en'];
        return $data;
    }
    
    
    
}
