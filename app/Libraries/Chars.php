<?php namespace App\Libraries;
/**
 * 文字处理类
 * @author d.x.c
 * @Created On Nov 5, 2015,3:56:34 PM
 */

use Config\Env;

class Chars {
    
    const INSPECT_USER              = 'user';
    const INSPECT_POST              = 'post';
    const INSPECT_MESSAGE           = 'message';
    
    /* 是否开启文字合法检查 */
    public static $inspectable      = true;
    
    
    public static function inspect($content, $scope, $lang){
        
        return true;
    }
    
}
