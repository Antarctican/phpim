<?php namespace App\Libraries;

/**
 * 处理地区的类
 * @author d.x.c
 * @Created On Sep 17, 2015,9:34:17 AM
 */
class Locale {
    
    const COUNTRY_DEFAULT                = 'JP';
    const LANG_DEFAULT                  = 'ja-JP';
    
    private static $isoContries         = [
        'US' => 'United States',
        'CA' => 'Canada',
        'AU' => 'Austrilia',
        'NZ' => 'New Zealand',
        'UK' => 'United Kingdom',
        'FR' => 'France',
        'DE' => 'Germany',
        'ES' => 'Spain',
        'IT' => 'Italy',
        'BE' => 'Belgium',
        'NL' => 'Netherlands',
        'Lu' => 'Luxembourg',
        'CH' => 'Switzerland',
        'SE' => 'Sweden',
        'NO' => 'Norway',
        'FI' => 'Finland',
        'DK' => 'Denmark',
        'IE' => 'Ireland',
        'IS' => 'Iceland',
        'AT' => 'Austria',
        'HU' => 'Hungary',
        'CZ' => 'Czech',
        'SK' => 'Slovakia',
        'PT' => 'Portugal',
        'TR' => 'Turkey',
        'GR' => 'Greece',
        'CY' => 'Cyprus',
        'RO' => 'Romania',
        'BG' => 'Bulgaria',
        'SI' => 'Slovenia',
        'RS' => 'Serbia',
        'ME' => 'Montenegro',
        'PL' => 'Poland',
        'UA' => 'Ukraine',
        'RU' => 'Russia',
        'AM' => 'Armenia',
        'GE' => 'Georgia',
        'BY' => 'Belarus',
        'LV' => 'Latvia',
        'LT' => 'Lithuania',
        'AZ' => 'Azerbaijan',
        'CN' => 'China',
        'JP' => 'Japan',
        'KR' => 'Korea',
        'HK' => 'Hongkong',
        'TW' => 'Taiwan',
        'MY' => 'Malaysia',
        'SG' => 'Singapore',
        'TH' => 'Thailand',
        'ID' => 'Indonesia',
        'PH' => 'Philippines',
        'VN' => 'Vietnam',
        'MM' => 'Myanmar',
        'IN' => 'India',
        'PK' => 'Pakistan',
        'LK' => 'Sri Lanka',
        'BD' => 'Bangladesh',
        'NP' => 'Nepal',
        'KZ' => 'Kazakhstan',
        'UZ' => 'Uzbekistan',
        'KG' => 'Kyrgyzstan',
        'TJ' => 'Tajikistan',
        'TM' => 'Turkmenistan',
        'AE' => 'United Arab Emirates',
        'IL' => 'Israel',
        'IR' => 'Iran',
        'IQ' => 'Iraq',
        'SA' => 'Saudi Arabia',
        'QA' => 'Qatar',
        'KW' => 'Kuwait',
        'JO' => 'Jordan',
        'LB' => 'Lebanon',
        'YE' => 'Yemen',
        'OM' => 'Oman',
        'MX' => 'Mexico',
        'AR' => 'Argentina',
        'BR' => 'Brazil',
        'PE' => 'Peru',
        'CL' => 'Chile',
        'CO' => 'Colombia',
        'CU' => 'Cuba',
        'VE' => 'Venezuela',
        'EG' => 'Egypt',
        'NG' => 'Nygeria',
        'LY' => 'Libya',
        'KE' => 'Kenya',
        'SD' => 'Sudan',
        'SS' => 'South Sudan',
        'ZA' => 'South Africa'
    ];
    
    
    public static $supportedLangs         = [
        'en-US' => 'English',
        'zh-CN' => '简体中文',
        'zh-TW' => '繁体中文',
        'ja-JP' => '日本語',
        'ko-KR' => '한글',
        /*
        'fr-FR' => 'Français',
        'de-DE' => 'Deutsch',
        'it-IT' => 'Italiano',
        'ru-Ru' => 'русский',
        'pt-BR' => 'Português',
        */
    ];
    
    public static function fixLanguage($lang){
        return $lang && isset(self::$supportedLangs[$lang]) ? $lang : self::LANG_DEFAULT;
    }
    
    public static function isoToCountry($iso){
        return isset(self::$isoContries[$iso]) ? self::$isoContries[$iso] : self::COUNTRY_DEFAULT;
    }
    
    /**
     * 根据语言code，  格式 zh-CN 取得所在国家
     * @param string $code
     */
    public static function getCountryByLangCode($code){
        $code = explode('-', $code);
        $code = isset($code[1]) ? strtoupper($code[1]) : self::COUNTRY_DEFAULT;
        return self::isoToCountry($code);
    }
    
}
