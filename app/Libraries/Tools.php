<?php namespace App\Libraries;
/**
 * @author d.x.c
 */

use Config\Env;

class Tools {
    
    public static function request($url, $method = 'GET', $fileds = '', $timeout = 10) {
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_RETURNTRANSFER  => TRUE,
            CURLOPT_FOLLOWLOCATION  => TRUE,
            CURLOPT_HEADER          => 0,
            CURLOPT_TIMEOUT         => $timeout,
            CURLOPT_USERAGENT       => Env::APP_NATIVE_USERAGENT,
        //    CURLOPT_ENCODING        => 'gzip, deflate',
            CURLOPT_SSL_VERIFYHOST  => false,
            CURLOPT_SSL_VERIFYPEER  => false,
        ));
        $url = strpos($url, 'http') === 0 ? $url : Env::BASE_URL.$url;
        if($method == 'POST'){
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fileds);
        }
        else{
            $url = $fileds ? $url.'?'.$fileds : $url;
            curl_setopt($ch, CURLOPT_URL, $url);
        }

        $content = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return ['content' => $content, 'code' => $code];
    }
    
    public static function GET($url){
        return self::request($url, 'GET');
    }
    
    public static function POST($url, $fileds){
        return self::request($url, 'POST', $fileds);
    }
    
    
    public static function saveToLocal($tmp_name, $name){
        $des = '/web/wwwroot/machi_storage/'.$name;
        if(move_uploaded_file($tmp_name, $des)){
            return true;
        }
        else{
            return false;
        }
    }
    
    
    public static function saveToS3($tmp_name, $name){
        
    }
    
    
}
