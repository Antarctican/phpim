<?php namespace App\Libraries;
/**
 * 序列字符产生类
 * @author d.x.c
 * @Created On Sep 26, 2015,11:51:29 AM
 */


class Generate {
    
    //private static $chars = '0123456789abcdefghijklmnopqrstuvwxyz';
    private static $chars = '60159jpsr83wmbhivqelat2zfn7odcxkgyu4';

    public static function uuid(){
        $s = md5(mt_rand(1,10000).microtime());
        return substr($s, 0, 5).'-'.substr($s, 5, 5).'-'.substr($s, 10, 5).'-'.substr($s, 15, 5);
    }
    
    
    public static function token($length = 30){
        $s = str_shuffle(self::$chars.uniqid());
        return substr($s, 2, $length);
    }
    
    public static function textID($pre = ''){
        $s = md5(microtime().mt_rand(-500,500));
        return date('ymdH-').substr($s, 0, 8).'-'.substr($s, -8);
    }
    
    
}
