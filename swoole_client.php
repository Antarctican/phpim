#!/web/lib/php7beta2/bin/php
<?php

class Client {

    private $clientent;

    public function __construct() {
        $this->client = new swoole_client(SWOOLE_SOCK_TCP, SWOOLE_SOCK_ASYNC);
        $this->client->on('Connect', array($this, 'onConnect'));
        $this->client->on('Receive', array($this, 'onReceive'));
        $this->client->on('Close', array($this, 'onClose'));
        $this->client->on('Error', array($this, 'onError'));
    }

    public function connect() {
        if (!($fp = $this->client->connect("127.0.0.1", 9501, 1))) {
            echo "Error: {$fp->errMsg}[{$fp->errCode}]\n";
            return false;
        }
        return true;
    }

    public function send($data) {
        return $this->client->send($data);
    }

    public function isConnected() {
        return $this->client->isConnected();
    }

    
    
    ##== works ====================##
    
    public function onReceive($client, $data) {
        echo "Get Message From Server: {$data}\n";
    }

    public function onConnect($client) {
        fwrite(STDOUT, "输入发送内容(json):");
        swoole_event_add(STDIN, function($fp){
            global $client;
            var_dump($fp);
            fwrite(STDOUT, "输入发送内容(json):");
            $msg = trim(fgets(STDIN));
            $client->send($msg);
        });
    }

    public function onClose($client) {
        echo "Client close connection\n";
    }

    public function onError() {
        
    }

}

$client = new Client();
$client->connect();
