<?php

$swConfig = [
    'timeout'           => 3,  //select and epoll_wait timeout. 
//    'reactor_num'       => 2,    //reactor thread num, 默认＝CPU核数
//    'writer_num'        => 2,    //writer thread num
    'worker_num'        => 2,     //worker process num, 代码异步，CPU核数的 1~4 倍即可; 同步阻塞设 100+
    'backlog'           => 128,     //listen backlog，最大等待accept的连接数量
    'max_request'       => 1000,    // 进程的最大存活请求数，超出将退出并重启进程
    'max_conn'          => 500,     //最大连接
    'dispatch_mode'     =>2,        //worker进程数据包分配模式, 1平均分配，2按FD取摸固定分配，3抢占式分配，默认为取摸(dispatch=2)
    'daemonize'         => 0,        // 转入后台作为守护进程
#    'open_cpu_affinity' => 1,       // 启用CPU亲和设置
#    'open_tcp_nodelay'  => 1,       // 启用tcp_nodelay
#    'tcp_defer_accept'  => 5,       // 连接在约定秒数内并不会触发accept
#    'log_file'          => '/data/log/swoole.log',  输出错误与异常记录到文件， 默认打印到屏幕
//    'open_eof_check'     => true,    // 打开buffer，检测数据是否完整；相对于固定长度包
//    'package_eof'        => "\r\n\r\n",      //设置EOF
    
//    'heartbeat_idle_time'       => 3600,    // 连接最大闲置时间，超时关闭
//    'heartbeat_check_interval'  => 300,     // 检测间隔，
     'debug_mode'       => 0
];

$mysqlConfig = [
    
];


$redisConfig = [
    
];
