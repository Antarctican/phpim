<?php 

class Message {
    
    private $serv;
    private $userModel;
    
    public function __construct() {
        
    }
    
    public function onStart($serv, $fd){
        echo '已开启';
    }
    
    public function onConnect($serv, $fd){
        $serv->send($fd, 'Connected');
        $serv->tick(10000, function() use ($serv, $fd) {
            $serv->send($fd, "hello~\n");
        });
    }
    
    public function onReceive($serv, $fd, $from_id, $data){
        var_dump($data);
        $serv->send($fd, '收到:'. $data );
    }
    
    public function onClose($serv, $fd){
        $serv->send($fd, '连接已关闭');
    
    }
    
    
    
    public function addMessage(){
        
    }
    
    
}