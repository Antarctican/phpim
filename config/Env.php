<?php namespace Config;
/**
 * 项目环境配置项 与 辅助方法类
 * @author d.x.c
 * @Created On Sep 26, 2015,11:44:14 AM
 */
class Env {
    
    /* 是否是免费版本 */
    const IS_FREE               = 1;
    /* 开发环境模式 */
    const IS_DEVELOPMENT        = 1;
    /* sandbox 模式 */
    const IS_SANDBOX            = 1;
    /* 打开log */
    const IS_RUN_LOG_ENABLED    = 1;
    
    
    /* 项目根URL， 以 / 结束 */
    const BASE_URL              = 'http://im.local:80/v1/';
    /* 允许请求的 User Agent */
    const APP_NATIVE_USERAGENT  = '';
    /* 发起本地系统请求使用的密钥，防止外部访问这些接口 */
    const SYS_REQUEST_KEY       = 'Yabantulinux!wro';
    /* 时区 */
    const TIMEZONE              = 'Asia/Tokyo';
    
    /* == APIs ===================================================== */
    /* AWS S3 */
    const AWS_ACCESS_KEY_ID     = 'AKIAJJFSQAWEBSHETYRQ';
    const AWS_SECRET_ACCESS_KEY = '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';
    const AWS_BUCKET_NAME       = 'phpim-res';
    const AWS_REGION            = 'ap-northeast-1';
    
    /* == 运行变量 ==================================================== */
    const OFFICIAL_USER_UID     = 1; //主键id
    const OFFICIAL_USER_ID      = 'THIS-IS-OFFICIAL'; //字符串ID
    
    /* post && comment 频率限制, xxx_SEC + xxx_NUM：n秒内不允许发布超过x条 */
    const POST_FREQUNCY_SEC     = 300;
    const POST_FREQUNCY_NUM     = 5;
    const COMMENT_FREQUNCY_SEC  = 60;
    const COMMENT_FREQUNCY_NUM  = 4;
    
    /* 虚拟用户 */
    const VIRTUAL_USER_TOTAL                = 0;
    const VIRTUAL_USER_ACTIVE_GRADE         = 0;
    
    /* 续费vip时， 赠送的vip时间秒数(86400的整数倍) */
    const RENEW_BONUS_VIP_EXTENT    = 0; 
    const RENEW_BONUS_SVIP_EXTENT   = 2592000; 
    
    /* 付费购买的物品 */
    public static $storeItems   = [
        'com.aaa.xx'    => ['price' => 90, 'value' => 0],
    ];
    
    /* == 文字过滤 ==================================================== */
    public static $inspectWords = [
        'user'  => [
            'en-US' => [],
            'zh-CN' => [],
            'zh-TW' => ["賴", "vip", "points", "line", "fb", "facebook", "telplone", "看不到", "看不见", "留言", "不知", "不要", "對不起", 
                        "不好意思", "炒飯", "愛愛", "做愛", "炮", "訊息", "對話", "騙", "錢", "加", "鎖", "啥", "胸","email", "兼差", "收入", 
                        "app", "茶", "外約", "id", "can't", "cant", "cannot", "歉", "sorry", "sori", "不太", "不大 ", "wechat", "微信", 
                        "we chart", "wechart", "無法", "酒店", "經紀", "沒法", "援交", "各取", "shit", "fuck", "gash", "mycard", "男"],
            'ja-JP' => [],
            'ko-KR' => [],
        ],
        'post'  => [
            'en-US' => [],
            'zh-CN' => [],
            'zh-TW' => ["賴", "vip", "點數", "line", "fb", "facebook","電話", "看不到", "看不見", "留言", "不知", "不要", "對不起", "不好意思",
                        "炒飯", "愛愛", "做愛", "炮", "訊息", "對話", "騙", "錢", "加", "鎖", "啥", "胸", "email", "兼差", "收入", "app", "茶", 
                        "外約", "傳", "爛", "自動", "id", "can't", "cant", "cannot", "歉", "sorry", "sori", "不太", "不大 ", "wechat", "微信",
                        "we chart", "wechart", "無法"],
            'ja-JP' => [],
            'ko-KR' => [],
        ],
        'message'  => [
            'en-US' => [],
            'zh-CN' => [],
            'zh-TW' => ["賴", "vip", "點數", "line", "fb", "facebook", "電話", "看不到", "看不見", "留言", "不知", "不要", "對不起", "不好意思", 
                        "炒飯", "愛愛", "做愛", "炮", "訊息", "對話", "騙", "錢", "加", "鎖", "啥", "胸", "email", "兼差", "收入", "app", "茶", 
                        "外約", "id", "can't", "cant", "cannot", "歉", "sorry", "sori", "不太", "不大 ", "wechat", "微信", "we chart", 
                        "wechart", "無法", "酒店", "經紀", "沒法", "援交", "各取", "shit", "fuck", "gash", "mycard", "男"],
            'ja-JP' => [],
            'ko-KR' => [],
        ],
    ];
    
    
    
    /**
     * 根据密钥判断是否是有效的系统请求
     * @param string $key     密钥
     * @return boolean
     */
    public static function isValidAdminRequest($key){
        return $key === self::SYS_REQUEST_KEY;
    }
    
    
    public static function url($uri){
        return self::BASE_URL . ltrim($uri, '/');
    }
    
}
