#!/web/lib/php7beta2/bin/php
<?php if(PHP_SAPI !== 'cli'){ die('Rejected');}
include 'swoole/message.php';
include 'swoole/ini.php';

class Server{
    
    private $serv;
    private $msg;
    
    public function __construct($swConfig) {
        $this->msg = new Message();
        $this->serv = new swoole_server('0.0.0.0', 9501, SWOOLE_BASE, SWOOLE_SOCK_TCP);
        $this->serv->set($swConfig);
        $this->serv->on('start', [$this->msg, 'onStart']);
        $this->serv->on('connect', [$this->msg, 'onConnect']);
        $this->serv->on('receive', [$this->msg, 'onReceive']);
        $this->serv->on('close', [$this->msg, 'onClose']);
        $this->serv->start();
    }
    
    public function onStart($serv, $fd){
        echo 'start';
    }
    
    public function onConnect($serv, $fd){
        echo 'connected';
    }
    
    public function onReceive($serv, $fd, $from_id, $data){
        var_dump($data);
        $serv->send($fd, '收到:'. $data );
    }
    
    public function onClose($serv, $fd){
        $serv->send($fd, '连接已关闭');
    }
    
}

$server = new Server($swConfig);





/*
$serv->on('connect', function($serv, $fd){
    $serv->send($fd, '已连接');
    $serv->tick(5000, function() use ($serv, $fd) {
        $serv->send($fd, "hello~\n");
    });
});
$serv->on('receive', function($serv, $fd, $from_id, $data){
    $serv->send($fd, $from_id. '收到:'. $data);
});
$serv->on('close', function($serv, $fd){
    $serv->send($fd, '连接已关闭');
});
$serv->start();
 * 
 */