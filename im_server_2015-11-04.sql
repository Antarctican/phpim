# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 127.0.0.1 (MySQL 5.6.21)
# Database: im_server
# Generation Time: 2015-11-04 05:59:56 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table circulation_value
# ------------------------------------------------------------

DROP TABLE IF EXISTS `circulation_value`;

CREATE TABLE `circulation_value` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fromUid` int(40) NOT NULL COMMENT '流出者id',
  `toUid` varchar(40) NOT NULL DEFAULT '' COMMENT '流入者id',
  `value` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '价值',
  `changeTime` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `fromUserID_2` (`fromUid`,`toUid`),
  KEY `fromUserID` (`fromUid`),
  KEY `toUserID` (`toUid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table device
# ------------------------------------------------------------

DROP TABLE IF EXISTS `device`;

CREATE TABLE `device` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userID` varchar(50) DEFAULT NULL,
  `deviceID` varchar(50) DEFAULT NULL COMMENT '设备ID',
  `deviceType` varchar(15) DEFAULT NULL COMMENT '设备类型',
  `accessToken` varchar(50) DEFAULT NULL COMMENT '访问Token',
  `lang` varchar(10) DEFAULT NULL,
  `version` tinytext COMMENT '客户端版本',
  `ip` varchar(16) DEFAULT NULL,
  `loginTime` int(11) DEFAULT '0' COMMENT '设备最近登录时间',
  PRIMARY KEY (`id`),
  KEY `DEVICE_UID` (`userID`),
  KEY `DEVICE_ID_TOKEN` (`deviceID`,`accessToken`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `device` WRITE;
/*!40000 ALTER TABLE `device` DISABLE KEYS */;

INSERT INTO `device` (`id`, `userID`, `deviceID`, `deviceType`, `accessToken`, `lang`, `version`, `ip`, `loginTime`)
VALUES
	(4,'2015-0123-56276b7a145ea','yuuuuuuuuu','WIN','5627864b374b0456789ABCDEF',NULL,'0.1.1','127.0.0.1',1445430859),
	(5,'2015-0123-56276b7a145ea','yuuuuuuuuu','WIN','5627864d7b2a1456789ABCDEF',NULL,'0.1.1','127.0.0.1',1445430861),
	(6,'2015-0123-56276b7a145ea','yuuuuuuuuu','WIN','562786cc37b82456789ABCDEF',NULL,'0.1.1','127.0.0.1',1445430988),
	(7,'2015-0123-56276b7a145ea','yuuuuuuuuu','WIN','562786d7c43c8456789ABCDEF',NULL,'0.1.1','127.0.0.1',1445430999),
	(13,'560664d95f8b0','ID1-DVC-4','iTunes','562877bb79763456789ABCDEF',NULL,'0.1.1','127.0.0.1',1445492667),
	(14,'560664d95f8b0','ID1-DVC-5','iPad','56287aa3211eb456789ABCDEF',NULL,'0.1.1','127.0.0.1',1445493411),
	(15,'560664d95f8b0','ID1-DVC-6','iPad','56287aadb9c12456789ABCDEF',NULL,'0.1.1','127.0.0.1',1445493421),
	(16,'560664d95f8b0','ID1-DVC-1','ANDROID','56287abed0a46456789ABCDEF',NULL,'0.1.1','127.0.0.1',1445493438),
	(17,'560664d95f8b0','ID1-DVC-2','ANDROID','56287ad81335a456789ABCDEF',NULL,'0.1.1','127.0.0.1',1445493464),
	(18,'2015-0123-56276b7a145ea','darvin-dvc-1','IOS','5628913a2498f45678',NULL,'2.0.0','127.0.0.1',1445499194),
	(19,'560666af707ab','haha2-dvc-1','IOS','562892bb9eafb45678',NULL,'2.0.0','127.0.0.1',1445499579),
	(20,'560667d1239cb','hahaha-dvc-1','IOS','5628935cdb62f45678',NULL,'2.0.0','127.0.0.1',1445499740);

/*!40000 ALTER TABLE `device` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table follow
# ------------------------------------------------------------

DROP TABLE IF EXISTS `follow`;

CREATE TABLE `follow` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `aUid` int(11) NOT NULL DEFAULT '0' COMMENT 'uid 1(较小)',
  `bUid` int(11) NOT NULL DEFAULT '0' COMMENT 'uid 2(较大)',
  `a2b` int(1) DEFAULT '0' COMMENT 'a是否关注b',
  `a2bTime` int(11) DEFAULT '0' COMMENT 'a关注b的时间',
  `b2a` int(1) DEFAULT '0' COMMENT 'b是否关注a',
  `b2aTime` int(11) DEFAULT '0' COMMENT 'b关注a的时间',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_interest_pair` (`aUid`,`bUid`),
  KEY `from_user_id` (`aUid`),
  KEY `to_user_id` (`bUid`),
  KEY `isMutual` (`a2b`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `follow` WRITE;
/*!40000 ALTER TABLE `follow` DISABLE KEYS */;

INSERT INTO `follow` (`id`, `aUid`, `bUid`, `a2b`, `a2bTime`, `b2a`, `b2aTime`)
VALUES
	(1,1,228,0,0,1,1446190288),
	(2,1,231,0,0,1,1446190348),
	(3,230,231,1,1446191838,0,0),
	(4,231,233,1,1446191857,1,1446191845),
	(6,231,228,1,1446191917,1,1446191917),
	(7,228,233,1,1446436867,1,1446436882);

/*!40000 ALTER TABLE `follow` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table message
# ------------------------------------------------------------

DROP TABLE IF EXISTS `message`;

CREATE TABLE `message` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `messageID` varchar(255) CHARACTER SET utf8 NOT NULL,
  `owner` varchar(255) NOT NULL,
  `sender` varchar(40) NOT NULL DEFAULT '',
  `receiver` varchar(255) NOT NULL DEFAULT '',
  `type` varchar(255) NOT NULL DEFAULT '',
  `content` varchar(4096) NOT NULL DEFAULT '',
  `createdAt` int(11) NOT NULL DEFAULT '0',
  `isDeleted` tinyint(1) NOT NULL DEFAULT '0',
  `isRead` tinyint(1) NOT NULL DEFAULT '0',
  `isPending` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `timestamp` (`createdAt`),
  KEY `sender` (`sender`),
  KEY `receiver` (`receiver`(191)),
  KEY `owner` (`owner`(191)),
  KEY `message_id` (`messageID`),
  KEY `is_deleted` (`isDeleted`),
  KEY `isRead` (`isRead`),
  KEY `needCheck` (`isPending`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table post
# ------------------------------------------------------------

DROP TABLE IF EXISTS `post`;

CREATE TABLE `post` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `postID` varchar(40) NOT NULL DEFAULT '',
  `content` text NOT NULL COMMENT '正文',
  `picture` tinytext COMMENT '索引图',
  `authorUid` int(11) NOT NULL COMMENT '作者uid',
  `country` varchar(50) DEFAULT '' COMMENT '国家/地区',
  `province` varchar(50) DEFAULT '' COMMENT '省/州/邦',
  `city` varchar(50) DEFAULT '' COMMENT '城市',
  `createdAt` int(11) NOT NULL DEFAULT '0' COMMENT '发布时间',
  `viewCount` int(11) NOT NULL DEFAULT '0' COMMENT '阅读数量',
  `likeCount` int(11) NOT NULL DEFAULT '0' COMMENT 'like数量',
  `commentCount` int(11) NOT NULL DEFAULT '0' COMMENT '评论数量',
  `isHidden` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否隐藏',
  `isChecked` tinyint(11) DEFAULT NULL COMMENT '是否已初审过',
  `isReviewed` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否评审',
  PRIMARY KEY (`id`),
  KEY `timestamp` (`createdAt`),
  KEY `user_id` (`authorUid`),
  KEY `like_count` (`likeCount`),
  KEY `comment_count` (`commentCount`),
  KEY `postID` (`postID`),
  KEY `isHidden` (`isHidden`),
  KEY `reviewed` (`isReviewed`),
  KEY `viewCount` (`viewCount`),
  KEY `country` (`country`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table post_comment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `post_comment`;

CREATE TABLE `post_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `commentID` varchar(40) NOT NULL DEFAULT '' COMMENT '评论ID',
  `postId` varchar(40) NOT NULL DEFAULT '' COMMENT 'Post id',
  `authorUid` varchar(40) NOT NULL DEFAULT '' COMMENT '作者uid',
  `content` tinytext NOT NULL COMMENT '正文',
  `isChecked` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否初审',
  `isReviewed` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否评审(人工)',
  `createdAt` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `post_id` (`postId`),
  KEY `timestamp` (`createdAt`),
  KEY `user_id` (`authorUid`),
  KEY `commentID` (`commentID`),
  KEY `needCheck` (`isChecked`),
  KEY `reviewed` (`isReviewed`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table report
# ------------------------------------------------------------

DROP TABLE IF EXISTS `report`;

CREATE TABLE `report` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `informerUid` int(11) DEFAULT '0' COMMENT '举报者id。系统:0',
  `accusedUid` int(11) DEFAULT '0' COMMENT '被举报者id',
  `commentID` int(11) DEFAULT '0' COMMENT '违规评论的id',
  `postID` int(11) DEFAULT '0' COMMENT '违规post的id',
  `reason` tinytext COMMENT '原因',
  `level` tinyint(4) DEFAULT '1' COMMENT '违规等级',
  `isAdminAction` tinyint(1) DEFAULT '0' COMMENT '是否是系统检测到的',
  `isProcessed` tinyint(11) DEFAULT '0' COMMENT '是否已处理',
  `createdAt` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



# Dump of table user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `userID` varchar(50) CHARACTER SET utf8 NOT NULL COMMENT '用户唯一ID',
  `email` varchar(60) DEFAULT '',
  `password` varchar(40) CHARACTER SET utf8 DEFAULT '',
  `nickname` varchar(40) DEFAULT NULL,
  `accessToken` varchar(50) DEFAULT NULL COMMENT '访问Token',
  `token` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `deviceID` varchar(50) CHARACTER SET utf8 DEFAULT NULL COMMENT '设备唯一ID',
  `deviceType` varchar(30) CHARACTER SET utf8 DEFAULT NULL COMMENT '设备类型',
  `avatar` varchar(50) CHARACTER SET utf8 DEFAULT '' COMMENT '头像',
  `album` text COMMENT '相册',
  `latitude` float DEFAULT '0' COMMENT '纬度',
  `longitude` float DEFAULT '0' COMMENT '经度',
  `gender` varchar(40) DEFAULT '0' COMMENT '性别. male, famale',
  `age` int(3) DEFAULT '0',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `constellation` varchar(40) DEFAULT '' COMMENT '星座',
  `height` int(11) DEFAULT '0',
  `weight` int(11) NOT NULL DEFAULT '0',
  `intro` varchar(1000) NOT NULL DEFAULT '' COMMENT '介绍',
  `job` varchar(1000) NOT NULL,
  `city` varchar(40) CHARACTER SET utf8 NOT NULL DEFAULT '' COMMENT '城市',
  `province` varchar(50) NOT NULL DEFAULT '' COMMENT '省/州/邦',
  `country` varchar(50) NOT NULL DEFAULT '' COMMENT '国家',
  `language` varchar(50) NOT NULL DEFAULT '' COMMENT '语言',
  `income` tinytext COMMENT '收入',
  `interest` varchar(1000) NOT NULL DEFAULT '' COMMENT '兴趣',
  `point` int(11) DEFAULT '0' COMMENT '积分额',
  `coin` int(11) DEFAULT '0' COMMENT '虚拟货币',
  `loginTime` int(11) NOT NULL DEFAULT '0' COMMENT '最近登录时间',
  `registerTime` int(11) NOT NULL DEFAULT '0' COMMENT '注册时间',
  `updatePhotoTime` int(11) NOT NULL DEFAULT '0' COMMENT '照片更新时间',
  `updateInfoTime` int(11) NOT NULL DEFAULT '0' COMMENT '资料更新时间',
  `isVerified` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否验证过身份',
  `isCelebrity` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否名人',
  `isHidden` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否隐藏',
  `isBlocked` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否冻结',
  `isPhotoChecked` tinyint(1) NOT NULL DEFAULT '0' COMMENT '照片是否审核过',
  `isReviewed` tinyint(1) NOT NULL DEFAULT '0' COMMENT '资料是否审核',
  `vipExpireAt` int(11) DEFAULT '0' COMMENT 'vip过期时间',
  `vipPlatinumExpireAt` int(11) DEFAULT '0' COMMENT '白金vip过期时间',
  `bid` int(11) NOT NULL DEFAULT '0' COMMENT '竞价值',
  `bidTime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '竞价时间',
  `visitCount` int(11) NOT NULL DEFAULT '0' COMMENT '总访问量',
  `visitTodayCount` int(11) NOT NULL DEFAULT '0' COMMENT '今日访问量',
  `visit7daysCount` int(11) DEFAULT '0' COMMENT '7日访问量',
  `visit30daysCount` int(11) DEFAULT '0' COMMENT '30天访问量',
  `visitCountHistory` text COMMENT '每日访问历史计数',
  `pushNotification` tinyint(1) NOT NULL DEFAULT '1',
  `pushMessage` tinyint(1) NOT NULL DEFAULT '1',
  `pushInterest` tinyint(1) NOT NULL DEFAULT '1',
  `pushPostComment` tinyint(1) NOT NULL DEFAULT '1',
  `badge` int(11) NOT NULL DEFAULT '0' COMMENT '徽章',
  `badgeVisit` int(11) NOT NULL DEFAULT '0' COMMENT '访问徽章',
  `badgeFollow` int(11) NOT NULL DEFAULT '0' COMMENT '关注徽章',
  `badgeMutualFollow` int(11) NOT NULL DEFAULT '0' COMMENT '相互关注徽章',
  `badgePost` int(11) NOT NULL DEFAULT '0' COMMENT '发动态徽章',
  `autoBid` int(11) NOT NULL DEFAULT '0',
  `autoBidTime` int(11) NOT NULL DEFAULT '0',
  `followerCount` int(11) NOT NULL DEFAULT '0',
  `sentGiftValue` int(11) NOT NULL DEFAULT '0',
  `receivedGiftValue` int(11) NOT NULL DEFAULT '0',
  `replyRate` float NOT NULL DEFAULT '0',
  `receivedMsgCount` int(11) NOT NULL DEFAULT '0',
  `lastAutoSayHi` int(11) NOT NULL DEFAULT '0',
  `lastReceiveVipPoints` int(11) NOT NULL DEFAULT '0',
  `lastSendVipAutoSayHi` int(11) NOT NULL DEFAULT '0',
  `nextSayHiTime` int(11) NOT NULL DEFAULT '0',
  `autoSayHiReceivedCount` int(11) NOT NULL DEFAULT '0',
  `vipFirstPurchaseTime` int(11) NOT NULL DEFAULT '0',
  `vipPurchasedMonthCount` int(11) NOT NULL DEFAULT '0',
  `platinumVipFirstPurchaseTime` int(11) NOT NULL DEFAULT '0',
  `platinumVipPurchasedMonthCount` int(11) NOT NULL DEFAULT '0',
  `needGiveRenewVipBonus` int(11) NOT NULL DEFAULT '0' COMMENT '在此时间之前，续费可获得vip红包',
  `needSendVipRenewPrompt` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否需要发送vip延续通知',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `userID` (`userID`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `deviceID` (`deviceID`),
  KEY `password` (`password`),
  KEY `age` (`age`),
  KEY `gender` (`gender`),
  KEY `longitude` (`longitude`),
  KEY `latitude` (`latitude`),
  KEY `bid` (`bid`),
  KEY `bid_time` (`bidTime`),
  KEY `vip_expiration` (`vipExpireAt`),
  KEY `is_freezed` (`isBlocked`),
  KEY `daily_visit_count` (`visitTodayCount`),
  KEY `visit_count` (`visitCount`),
  KEY `city` (`city`),
  KEY `need_check_picture` (`isPhotoChecked`),
  KEY `check_picture_time` (`updatePhotoTime`),
  KEY `picture` (`avatar`),
  KEY `last_login` (`loginTime`),
  KEY `accessToken` (`accessToken`),
  KEY `accessToken_2` (`accessToken`),
  KEY `isCelebrity` (`isCelebrity`),
  KEY `point` (`point`),
  KEY `autoBid` (`autoBid`),
  KEY `autoBidTime` (`autoBidTime`),
  KEY `lastAutoSayHi` (`lastAutoSayHi`),
  KEY `followerCount` (`followerCount`),
  KEY `sendGiftCount` (`sentGiftValue`),
  KEY `machiPoint` (`coin`),
  KEY `receivedGiftValue` (`receivedGiftValue`),
  KEY `reviewed` (`isReviewed`),
  KEY `updateInfoTime` (`updateInfoTime`),
  KEY `job` (`job`(191)),
  KEY `intro` (`intro`(191)),
  KEY `interest` (`interest`(191)),
  KEY `deviceType` (`deviceType`),
  KEY `replyRate` (`replyRate`),
  KEY `receivedMsgCount` (`receivedMsgCount`),
  KEY `lastReceiveVipPoints` (`lastReceiveVipPoints`),
  KEY `lastSendVipAutoSayHi` (`lastSendVipAutoSayHi`),
  KEY `autoSayHiReceivedCount` (`autoSayHiReceivedCount`),
  KEY `vipRenewMonthCount` (`vipPurchasedMonthCount`),
  KEY `vipFirstPurchaseTime` (`vipFirstPurchaseTime`),
  KEY `platinumVipExpiration` (`vipPlatinumExpireAt`),
  KEY `platinumVipPurchasedMonthCount` (`platinumVipPurchasedMonthCount`),
  KEY `platinumVipFirstPurchaseTime` (`platinumVipFirstPurchaseTime`),
  KEY `needGiveRenewVipBonus` (`needGiveRenewVipBonus`),
  KEY `needSendVipRenewPrompt` (`needSendVipRenewPrompt`),
  KEY `country` (`country`),
  KEY `province` (`province`),
  KEY `nextSayHiTime` (`nextSayHiTime`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;

INSERT INTO `user` (`uid`, `userID`, `email`, `password`, `nickname`, `accessToken`, `token`, `deviceID`, `deviceType`, `avatar`, `album`, `latitude`, `longitude`, `gender`, `age`, `birthday`, `constellation`, `height`, `weight`, `intro`, `job`, `city`, `province`, `country`, `language`, `income`, `interest`, `point`, `coin`, `loginTime`, `registerTime`, `updatePhotoTime`, `updateInfoTime`, `isVerified`, `isCelebrity`, `isHidden`, `isBlocked`, `isPhotoChecked`, `isReviewed`, `vipExpireAt`, `vipPlatinumExpireAt`, `bid`, `bidTime`, `visitCount`, `visitTodayCount`, `visit7daysCount`, `visit30daysCount`, `visitCountHistory`, `pushNotification`, `pushMessage`, `pushInterest`, `pushPostComment`, `badge`, `badgeVisit`, `badgeFollow`, `badgeMutualFollow`, `badgePost`, `autoBid`, `autoBidTime`, `followerCount`, `sentGiftValue`, `receivedGiftValue`, `replyRate`, `receivedMsgCount`, `lastAutoSayHi`, `lastReceiveVipPoints`, `lastSendVipAutoSayHi`, `nextSayHiTime`, `autoSayHiReceivedCount`, `vipFirstPurchaseTime`, `vipPurchasedMonthCount`, `platinumVipFirstPurchaseTime`, `platinumVipPurchasedMonthCount`, `needGiveRenewVipBonus`, `needSendVipRenewPrompt`)
VALUES
	(1,'THIS-IS-OFFICIAL','Chatty@chattyim.com','','Chatty官方账号',NULL,NULL,NULL,NULL,'',NULL,0,0,'0',NULL,NULL,'',0,0,'','','','','','',NULL,'',0,0,1446027214,0,0,0,1,0,0,0,1,0,0,1446021762,0,0,0,0,0,0,NULL,1,1,1,1,0,0,2,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
	(228,'560664d95f8b0','haha@lala.com','cd6a1a15421189de23d7309feebff8d7','咖喱可以','56287ad81335a456789ABCDEF',NULL,'ID1-DVC-2','ANDROID','kkkkk.jpg','[\"hahahah.jpg\", \"hehehe.png\"]',104.074,30,'male',29,'1986-10-25','',0,0,'','','','','Japan','zh-TW','','',500,0,1446026995,1443259609,0,0,0,0,0,0,1,0,0,0,3,1445323408,2,2,2,2,'{\"2015-10-28\":2}',1,1,1,1,0,0,2,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
	(230,'560666af707ab','haha2@lala.com','cd6a1a15421189de23d7309feebff8d7','啤酒喔','560666af70c42','skksk','hehehe','iPhone','xxxxxxxxx.jpg',NULL,102,34,'male',24,'1990-10-28','',0,0,'','','chengdu','sichuan','china','ja-JP','','',500,0,1446027272,1443260079,0,0,0,0,0,0,1,0,0,0,1,1445323408,0,0,0,0,NULL,1,1,1,1,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
	(231,'560667d1239cb','hahaha@lala.com','cd6a1a15421189de23d7309feebff8d7','Colistina La\'t','560667d1239f6',NULL,'hehe55645','iPhone','xxxxxxxxx.jpg',NULL,102.4,34.22,'female',17,'1998-01-01','',0,0,'','','chengdu','sichuan','china','en-US',NULL,'',10000,0,1446027227,1443260369,0,0,0,0,0,0,1,0,0,0,5,1445323408,0,0,0,0,NULL,1,1,1,1,0,0,2,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),
	(233,'2015-0123-56276b7a145ea','Darvin@mi.com','cd6a1a15421189de23d7309feebff8d7','dfsdaf','562882b633e6d456789ABCDEF',NULL,'','','sfdasdf.jpg',NULL,103,32.5,'female',15,'2000-06-04','',0,0,'','','','','Japan','ja-JP',NULL,'',25,0,1445499194,1445423994,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,NULL,1,1,1,1,0,0,3,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);

/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table user_blacklist
# ------------------------------------------------------------

DROP TABLE IF EXISTS `user_blacklist`;

CREATE TABLE `user_blacklist` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `uid` int(11) DEFAULT '0' COMMENT '所属用户ID',
  `banUid` int(50) DEFAULT NULL COMMENT '被屏蔽userID',
  `createdAt` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `U_BLACKLIST_UID` (`uid`,`banUid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

LOCK TABLES `user_blacklist` WRITE;
/*!40000 ALTER TABLE `user_blacklist` DISABLE KEYS */;

INSERT INTO `user_blacklist` (`id`, `uid`, `banUid`, `createdAt`)
VALUES
	(3,228,231,1445600956);

/*!40000 ALTER TABLE `user_blacklist` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table visit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `visit`;

CREATE TABLE `visit` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fromUid` int(11) NOT NULL,
  `toUid` int(11) NOT NULL,
  `createdAt` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_visit_pair` (`fromUid`,`toUid`),
  KEY `visitor_user_id` (`fromUid`),
  KEY `visited_user_id` (`toUid`),
  KEY `timestamp` (`createdAt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `visit` WRITE;
/*!40000 ALTER TABLE `visit` DISABLE KEYS */;

INSERT INTO `visit` (`id`, `fromUid`, `toUid`, `createdAt`)
VALUES
	(4,1,228,1446027201),
	(8,230,228,2222);

/*!40000 ALTER TABLE `visit` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
